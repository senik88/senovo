<?php
/**
 * @var $content string
 */

use app\assets\DashboardAsset;
use yii\bootstrap\Html;
use yii\helpers\Url;

DashboardAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600' rel='stylesheet' type='text/css'>
        <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/favicon.ico" type="image/x-icon" />
<!--        <script src='https://www.google.com/recaptcha/api.js'></script>-->
        <?php $this->head() ?>
    </head>

    <body>
    <?php $this->beginBody() ?>

        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                            aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?= Yii::$app->homeUrl ?>"><?= Yii::$app->name ?></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <?php if (!Yii::$app->user->isGuest) { ?>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="<?= Url::to(['/cms/default/index']) ?>">Dashboard</a></li>
                        <li><a href="#">Settings</a></li>
                        <li><a href="#">Help</a></li>
                        <li><a href="#">Profile</a></li>
                        <li><a href="<?= Url::to(['/cms/default/logout']) ?>">Logout</a></li>
                    </ul>
                    <form class="navbar-form navbar-right">
                        <input type="text" class="form-control" placeholder="Search...">
                    </form>
                    <?php } ?>
                </div>
            </div>
        </nav>

        <?= \app\components\senovo\Flashes::widget() ?>

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3 col-md-2 sidebar">
                    <?php if (!Yii::$app->user->isGuest) { ?>
                    <ul class="nav nav-sidebar">
                        <li><a href="<?= Url::to(['/cms/menu/index']) ?>">Web menu</a></li>
                        <li><a href="<?= Url::to(['/cms/carousel/index']) ?>">Carousel</a></li>
                        <li><a href="<?= Url::to(['/cms/article/index']) ?>">Articles</a></li>
                        <li><a href="<?= Url::to(['/cms/reference/index']) ?>">References</a></li>
                        <li><a href="<?= Url::to(['/cms/settings/index']) ?>">Web settings</a></li>
                        <li><a href="<?= Url::to(['/cms/messages/index']) ?>">Messages</a></li>
                        <li><a href="<?= Url::to(['/cms/gallery/index']) ?>">Photogallery</a></li>
                        <li><a href="<?= Url::to(['/cms/files/index']) ?>">Uploaded files</a></li>
                    </ul>
                    <?php } ?>
                    <!--<hr />
                    <ul class="nav nav-sidebar">
                        <li><a href="">Nav item again</a></li>
                        <li><a href="">One more nav</a></li>
                        <li><a href="">Another nav item</a></li>
                    </ul>-->
                </div>

                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                    <!-- Vlastni obsah stranky -->
                    <?= $content ?>
                </div>
            </div>
        </div>


    <?php $this->endBody() ?>
    </body>

</html>
<?php $this->endPage() ?>