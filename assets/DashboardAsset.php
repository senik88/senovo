<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 23.11.2016
 * Time: 22:00
 */

namespace app\assets;


use yii\web\AssetBundle;

class DashboardAsset extends AssetBundle
{
    public $basePath = '@app/themes/dashboard';
    public $baseUrl = '@web/themes/dashboard';

    public $css = [
        'css/dashboard.css'
    ];

    public $js = [
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset'
    ];
}