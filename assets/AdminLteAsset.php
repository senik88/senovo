<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 23.11.2016
 * Time: 22:00
 */

namespace app\assets;


use yii\web\AssetBundle;

class AdminLteAsset extends AssetBundle
{
    public $basePath = '@app/themes/adminlte';
    public $baseUrl = '@web/themes/adminlte';

    public $css = [
    ];

    public $js = [
    ];

    public $depends = [
//        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapPluginAsset'
    ];
}