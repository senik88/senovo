<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 26.12.2018
 * Time: 12:05
 */

namespace app\assets;


use yii\web\AssetBundle;

/**
 * Class FancyBoxAsset
 * @package app\assets
 */
class FancyBoxAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'fancybox/jquery.fancybox.min.css',
    ];
    public $js = [
        'fancybox/jquery.fancybox.min.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}