/**
 * Created by Senik on 10.01.2016.
 */
(function(Senovo, $, undefined) {
    /**
     * bylo by vhodne tohle resit pres observer, aby se obrazky nacetly az budou zobrazeny
     */
    Senovo.renderArticleBackgrounds = function() {
        //window.console.log($('article.bg1'));
        var i, url, hash,
            articles = $('article.bg1'),
            $article;

        for (i = 0; i < articles.length; i++) {
            $article = $(articles[i]);
            hash = $article.attr('data-picture');

            if (hash.length > 0) {
                url = basePath + '/site/file?hash=' + hash;
                $article.css({
                    "background-image": "url('" + url +"')"
                });
            }
        }
    };
}(window.Senovo = window.Senovo || {}, jQuery));

(function ($) {
    $('a[data-scroll="top"]').on('click', function() {
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });

    $(document).on('load', Senovo.renderArticleBackgrounds());

    $('#more-references').on('click', function() {

        var offset = $('div.reference').length;
        var button = $('#more-references').closest('div.row');

        var request = $.ajax({
            url: basePath + '/site/reference', // todo routovani
            type: 'GET',
            data: {offset: offset}
        });

        request.done(function(data) {
            $('#reference-wrapper').append($(data.html).fadeIn('slow'));

            if (data.more == 0) {
                button.hide();
            }
        });
    });

    // skrolovani na kotvy
    $('li.menu-link a').on('click', function(event) {
        var anchor = $(this).closest('li').attr('data-anchor');

        if (anchor != undefined) {
            var object = $('#' + anchor);

            if (object.length) {
                event.preventDefault();

                $('html,body').animate({
                    scrollTop: object.offset().top
                }, "slow");
            }
        }
    });

    $('#contact-form').on('beforeSubmit', function(event) {
        event.preventDefault();

        var form = $(this);

        var request = $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: form.serialize()
        });

        request.done(function(data) {

        });
    }).on('submit', function(event) {
        event.preventDefault()
    });
})(jQuery);