<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 28.04.2018
 * Time: 17:43
 */

namespace app\components;

use app\models\File;
use yii\base\Component;
use yii\data\ArrayDataProvider;

/**
 * Class FileManager
 * @package app\components
 */
class FileManager extends Component
{
    protected $_path;

    protected $_root;

    /**
     *
     */
    public function init()
    {
        $this->_root = \Yii::getAlias('@runtime') . DIRECTORY_SEPARATOR . 'files';
    }

    /**
     * @param $path
     */
    public function setPath($path)
    {
        $this->_path = $this->normalizePath($path);
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->_path;
    }

    /**
     * @return string
     */
    public function getRelativePath()
    {
        $path = $this->getPath();

        if ($path == $this->_root) {
            return '';
        } else {
            return substr($path, strlen($this->_root));
        }
    }

    /**
     * todo tohle bude chtit jeste hodne poladit... hlavne co se tyce
     * @return array
     */
    public function getFiles()
    {
        $removeDots = ($this->_path == $this->_root);

        $files = $this->scandir($this->_path, $removeDots);

        $models = [];
        foreach ($files as $file) {
            $fullPath = $this->_path . DIRECTORY_SEPARATOR . $file;
            $info = pathinfo($fullPath);

            if (is_dir($fullPath)) {
                $models[] = new File([
                    'size' => 'Folder',
                    'name' => $file,
                    'path' => $file,
                    'uploaded' => null,
                    'mimetype' => mime_content_type($fullPath)
                ]);
            } else {
                if (($model = File::findOne($file)) !== null) {
                    $models[] = $model;
                } else {
                    // unlink?
                }
            }
        }

        return new ArrayDataProvider([
            'allModels' => $models
        ]);
    }

    /**
     * @param $path
     * @return string
     */
    private function normalizePath($path)
    {
        $normalized = $this->_root;

        if ($path != null) {
            $normalized .= DIRECTORY_SEPARATOR . $path;
        }

        return $normalized;
    }

    /**
     * @param $dir
     * @param bool $remove
     * @return array
     */
    private function scandir($dir, $remove = false)
    {
        $content = scandir($dir);

        $thisdir = '.';
        $key = array_search($thisdir, $content);
        if ($key !== false) {
            unset($content[$key]);
        }

        if ($remove) {
            $upperdir = '..';
            $key = array_search($upperdir, $content);
            if ($key !== false) {
                unset($content[$key]);
            }
        }

        return $content;
    }
}