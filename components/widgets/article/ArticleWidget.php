<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 24.04.2018
 * Time: 19:42
 */

namespace app\components\widgets\article;


use app\models\article\Article;
use yii\bootstrap\Html;
use yii\bootstrap\Widget;

/**
 * Class ArticleWidget
 * @package app\components\widgets\article
 */
class ArticleWidget extends Widget
{
    /**
     * @var Article
     */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('_article');
    }

    /**
     * @param string $view
     * @param array $params
     * @return string
     */
    public function render($view, $params = [])
    {
        $template = parent::render($view, $params);

        return strtr($template, [
            '{article_id}' => $this->model->article_id,
            '{content}' => $this->model->content,
            '{button_up}' => $this->model->show_up ? Html::a('Nahoru ' . Html::icon('arrow-up'), '#', ['data-scroll' => 'top']) : '',
            '{title}' => $this->model->title,
            '{picture}' => $this->model->picture,
            '{picture_pos}' => strtolower($this->model->picture_pos)
        ]);
    }
}