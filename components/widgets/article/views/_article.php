<article class="bg1" id="{article_id}" data-picture-position="{picture_pos}" data-picture="{picture}">
    <div class="row">
        <div class="article-content">
            <div class="arrow"></div>
            <div class="article-text">
                <div class="heading-buttons">
                    <h3>{title}</h3>
                    {button_up}
                    <div class="clearfix"></div>
                </div>
                {content}
            </div>
        </div>
        <div class="article-image"></div>
    </div>
    <div class="spacer"></div>
</article>