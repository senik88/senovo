<div class="row reference">
    <div class="col-md-12">
        <div class="reference-image">
            <!-- otaceni obrazku - https://davidwalsh.name/css-flip -->
            <img src="<?= $mReference->getType()->getIconFullPath() ?>">
        </div>
        <div class="reference-content">
            <p>
                <b><?= Yii::$app->formatter->asDate($mReference->ref_date) ?></b>
            </p>
            <?= $mReference->content ?>

            <?php
            if ($mReference->link != null) {
                echo Html::tag('p', Html::a($mReference->link, $mReference->link, ['target' => '_blank']));
            }
            ?>
        </div>
    </div>
</div>