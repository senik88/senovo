<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 16.12.2018
 * Time: 14:18
 */

namespace app\components\widgets\article;


use app\models\reference\Reference;
use yii\bootstrap\Widget;

/**
 * Class ReferenceWidget
 * @package app\components\widgets\article
 */
class ReferenceWidget extends Widget
{
    /**
     * @var Reference
     */
    public $model;

    /**
     * todo nacitat sablonu z konfigurace, at si muzu nahravat vlastni sablony
     * @return string
     */
    public function run()
    {
        return $this->render('_reference');
    }

    /**
     * @param string $view
     * @param array $params
     * @return string
     */
    public function render($view, $params = [])
    {
        $template = parent::render($view, $params);

        return strtr($template, [
//            '{article_id}' => $this->model->article_id,
//            '{content}' => $this->model->content,
//            '{button_up}' => $this->model->show_up ? Html::a('Nahoru ' . Html::icon('arrow-up'), '#', ['data-scroll' => 'top']) : '',
//            '{title}' => $this->model->title,
//            '{picture}' => $this->model->picture,
//            '{picture_pos}' => strtolower($this->model->picture_pos)
        ]);
    }
}