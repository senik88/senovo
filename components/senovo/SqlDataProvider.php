<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 02.04.2018
 * Time: 18:08
 */

namespace app\components\senovo;


use yii\base\InvalidConfigException;
use yii\db\Connection;

/**
 * Class SqlDataProvider
 * @package app\components
 */
class SqlDataProvider extends \yii\data\SqlDataProvider
{
    /**
     * @var bool jedna se o export gridu? ridim tim strankovani
     */
    public $export = false;

    /**
     * @return bool|null|string
     */
    protected function prepareTotalCount()
    {
        $yii = \Yii::$app;

        /** @var $connection Connection */
        $connection = (($this->db !== null) ? $this->db : $yii->db);

        $command = $connection->createCommand("SELECT count(*) FROM ({$this->sql}) AS subselect");
        $command->bindValues($this->params);
        $res = $command->queryScalar();

        return (int) $res;
    }
}