<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 01.04.2018
 * Time: 21:02
 */

namespace app\components\senovo;

/**
 * Class ActiveRecord
 * @package app\components\senovo
 */
class ActiveRecord extends \yii\db\ActiveRecord
{
    /**
     * @var Filter
     */
    protected $_filter;

    /**
     * @param $filter
     */
    public function setFilter($filter)
    {
        $this->_filter = $filter;
    }

    /**
     * @return false|null|string
     */
    public static function getNewPkey()
    {
//        $command = static::getDb()->createCommand(
//            "SELECT nextval(pg_get_serial_sequence(:table, :key))"
//            , [
//            ':table' => static::tableName(),
//            ':key' => self::primaryKey()[0]
//        ]);
//
//        $command->prepare();
//
//        $sql = $command->getSql();
//
//        \Kint::dump($sql); die;

        $command = static::getDb()->createCommand()->setSql("SELECT nextval(pg_get_serial_sequence('" . static::tableName() . "', '" . self::primaryKey()[0] . "'))");
        $pk = $command->queryScalar();

        return $pk;
    }
}