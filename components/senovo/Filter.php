<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 01.04.2018
 * Time: 20:57
 */

namespace app\components\senovo;

use yii\base\Model;

/**
 * Class Filter
 * @package app\components\senovo
 */
class Filter extends Model
{
    /**
     * @param $attr
     * @param null $defaultValue
     * @return mixed
     */
    public function getFilterVal($attr, $defaultValue = null)
    {
        return $this->$attr;
    }
}