<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 24.04.2018
 * Time: 21:55
 */

namespace app\components\senovo;

use Yii;
use yii\bootstrap\Widget;

/**
 * Class Flashes
 * @package app\components\senovo
 */
class Flashes extends Widget
{
    /**
     * @param $key
     * @param $message
     */
    protected static function addFlash($key, $message)
    {
        Yii::$app->session->addFlash($key, $message);
    }

    /**
     * @param $message
     */
    public static function addInfo($message)
    {
        self::addFlash('info', $message);
    }

    /**
     * @param $message
     */
    public static function addSuccess($message)
    {
        self::addFlash('success', $message);
    }

    public static function addError($message)
    {
        self::addFlash('danger', $message);
    }

    /**
     * @return string
     */
    public function run()
    {
        $messages = Yii::$app->session->getAllFlashes();

        return $this->render('flashes', ['allMessages' => $messages]);
    }
}