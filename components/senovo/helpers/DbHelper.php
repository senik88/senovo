<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 26.04.2018
 * Time: 21:14
 */

namespace app\components\senovo\helpers;


class DbHelper {

    /**
     * @param $table
     * @param $column
     * @return false|null|string
     */
    public static function newPkey($table, $column)
    {
        return \Yii::$app->db->createCommand("SELECT nextval(pg_get_serial_sequence(:table, :column));", [':table' => $table, ':column' => $column])->queryScalar();
    }

}