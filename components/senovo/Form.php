<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 02.04.2018
 * Time: 10:30
 */

namespace app\components\senovo;


use yii\base\Model;

/**
 * Class Form
 * @package app\components\senovo
 */
abstract class Form extends Model
{
    /**
     * @var
     */
    protected $_model;

    /**
     *
     */
    abstract public function process();

    /**
     * @return mixed
     */
    abstract public function loadValues();
}