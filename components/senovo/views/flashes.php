<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 24.04.2018
 * Time: 21:58
 *
 * @var $allMessages array
 */

use yii\helpers\Html;

foreach ($allMessages as $type => $messages) {
    foreach ($messages as $message) {
        echo \kartik\widgets\Growl::widget([
            'type' => $type,
            'title' => (!empty($message['title'])) ? Html::encode($message['title']) : 'Title Not Set!',
            'icon' => (!empty($message['icon'])) ? $message['icon'] : 'fa fa-info',
            'body' => $message,
            'showSeparator' => true,
            'delay' => 1, //This delay is how long before the message shows
            'pluginOptions' => [
                'delay' => (!empty($message['duration'])) ? $message['duration'] : 3000, //This delay is how long the message shows for
                'placement' => [
                    'from' => (!empty($message['positonY'])) ? $message['positonY'] : 'top',
                    'align' => (!empty($message['positonX'])) ? $message['positonX'] : 'right',
                ]
            ]
        ]);
    }
}