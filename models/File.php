<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 26.04.2018
 * Time: 20:46
 */

namespace app\models;


use app\components\senovo\ActiveRecord;

/**
 * Class File
 * @package app\models
 *
 * @property integer $file_pk
 * @property integer $size
 * @property string  $hash
 * @property string  $name
 * @property string  $path
 * @property string  $title
 * @property string  $uploaded
 * @property string  $mimetype
 */
class File extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function rules ()
    {
        return [
            [$this->attributes(), 'safe']
        ];
    }

    public function init()
    {
        parent::init();

//        if ($this->isNewRecord) {
//            \Kint::dump('is');
//        } else {
//            \Kint::dump('is not');
//        }
//
//        die;
    }

    /**
     * @param null $filename
     * @return string
     */
    public function getAbsolutePath($filename = null)
    {
        $path = \Yii::getAlias('@runtime') . DIRECTORY_SEPARATOR . $this->getRelativePath();
        if ($filename !== null) {
            $path .= DIRECTORY_SEPARATOR . $filename;
        }

        return $path;
    }

    /**
     * Nazev slozky, kam se uploaduji soubory
     * @return string
     */
    public function getRelativePath()
    {
        return 'files';
    }
}