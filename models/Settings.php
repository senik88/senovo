<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 09.01.2016
 * Time: 15:50
 */

namespace app\models;


use yii\base\Model;

/**
 * Class Settings
 * @package app\models
 */
class Settings extends Model
{

    /**
     * @param $account
     * @return Settings
     */
    public static function getForAccount($account)
    {
        return new self;
    }

    /**
     * @return int
     */
    public function getInitialReferencesCount()
    {
        return 3;
    }

}