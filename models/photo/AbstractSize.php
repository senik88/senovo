<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 25.12.2018
 * Time: 16:33
 */

namespace app\models\photo;


use app\models\File;
use Imagine\Image\ImageInterface;

/**
 * Class AbstractSize
 * @package app\models\photo
 *
 * Vezme ImageInterface a udela z nej File
 */
abstract class AbstractSize
{
    /** @var  integer */
    protected $_width;
    /** @var  integer */
    protected $_height;
    /** @var  ImageInterface */
    protected $_image;
    /** @var  File */

    /**
     * @return int
     */
    public function getWidth()
    {
        return $this->_width;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->_height;
    }

    /**
     * @return ImageInterface
     */
    public function getImage()
    {
        return $this->_image;
    }

    /**
     * @param ImageInterface $image
     */
    public function setImage($image)
    {
        $this->_image = $image;
    }


    public function processImage()
    {
        // neco, asi nainstancovat file

        if ($this instanceof PhotoResizable) {
            $this->resize();
        }

        // ulozit image

    }
}