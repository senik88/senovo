<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 17.12.2018
 * Time: 12:27
 */

namespace app\models\photo;

/**
 * Interface PhotoInterface
 * @package app\models\photo
 */
interface PhotoInterface
{
    /**
     * @return integer
     */
    public function getWidth();

    /**
     * @return integer
     */
    public function getHeight();
}