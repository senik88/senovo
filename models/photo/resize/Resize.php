<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 25.12.2018
 * Time: 17:42
 */

namespace app\models\photo\resize;


use app\models\photo\AbstractSize;
use app\models\photo\PhotoResizable;
use Imagine\Image\Box;
use Imagine\Image\ManipulatorInterface;

/**
 * Class Resize
 * @package app\models\photo\resize
 */
class Resize extends AbstractSize implements PhotoResizable
{
    /**
     * @inheritdoc
     */
    public function resize()
    {
        $i_width = $this->_image->getSize()->getWidth();
        $i_height = $this->_image->getSize()->getHeight();

        // velky obrazek zmensim na pozadovynou velikost
        // ale zachovam pomer stran
        if ($i_width > $i_height) {
            if ($this->_image->getSize()->getWidth() > $this->_width) {
                $this->_image->resize($this->_image->getSize()->widen($this->_width));
            }
        } else {
            if ($this->_image->getSize()->getHeight() > $this->_height) {
                $this->_image->resize($this->_image->getSize()->heighten($this->_height));
            }
        }

        $this->_image->thumbnail(new Box($this->_width, $this->_height), ManipulatorInterface::THUMBNAIL_OUTBOUND);
    }
}