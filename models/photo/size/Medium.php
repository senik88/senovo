<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 17.12.2018
 * Time: 12:07
 */

namespace app\models\photo\size;


use app\models\photo\AbstractSize;
use app\models\photo\resize\Resize;

/**
 * Class Medium
 * @package app\models\photo\size
 */
class Medium extends Resize
{
    /** @var int  */
    protected $_width = 640;
    /** @var int  */
    protected $_height = 640;
}