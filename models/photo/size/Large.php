<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 17.12.2018
 * Time: 12:07
 */

namespace app\models\photo\size;


use app\models\photo\AbstractSize;
use app\models\photo\resize\Resize;

/**
 * Class Large
 * @package app\models\photo\size
 */
class Large extends Resize
{
    /** @var int  */
    protected $_width = 960;
    /** @var int  */
    protected $_height = 960;

}