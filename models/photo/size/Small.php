<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 17.12.2018
 * Time: 12:06
 */

namespace app\models\photo\size;


use app\models\photo\AbstractSize;
use app\models\photo\resize\Resize;

/**
 * Class Small
 * @package app\models\photo\size
 */
class Small extends Resize
{
    /** @var int  */
    protected $_width = 320;
    /** @var int  */
    protected $_height = 320;

}