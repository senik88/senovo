<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 17.12.2018
 * Time: 12:07
 */

namespace app\models\photo\size;


use app\models\photo\AbstractSize;

/**
 * Class Original
 * @package app\models\photo\size
 */
class Original extends AbstractSize
{
    /**
     * @return int
     */
    public function getWidth()
    {
        return $this->getImage()->getSize()->getWidth();
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->getImage()->getSize()->getHeight();
    }
}