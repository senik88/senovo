<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 17.12.2018
 * Time: 12:07
 */

namespace app\models\photo\size;


use app\models\photo\AbstractSize;
use app\models\photo\PhotoResizable;
use Imagine\Image\Box;
use Imagine\Image\Palette\RGB;
use Imagine\Image\Point;
use Imagine\Imagick\Imagine;

/**
 * Class Thumbnail
 * @package app\models\photo\size
 */
class Thumbnail extends AbstractSize implements PhotoResizable
{
    /** @var int  */
    protected $_height = 100;
    /** @var int  */
    protected $_width = 100;

    /**
     * @inheritdoc
     */
    public function resize()
    {
        $i_width = $this->_image->getSize()->getWidth();
        $i_height = $this->_image->getSize()->getHeight();

        $orgSize = $this->_image->getSize();

        // ale nahled chci mit ctvercovy, takze to musim cele prepocitat
        $target = new Box($this->_width, $this->_height);

        if ($i_width > $i_height) {
            $h = $orgSize->getHeight() * ($target->getWidth() / $orgSize->getWidth());
            $cropBy = new Point( 0, ( max($target->getHeight() - $h , 0 ) ) / 2);
        } else {
            $w = $orgSize->getWidth() * ( $target->getHeight() / $orgSize->getHeight() );
            $cropBy = new Point( ( max ($target->getWidth() - $w, 0 ) ) / 2, 0);
        }

        $palette = new RGB();
        $color = $palette->color('#FFF', 100);

        if ($i_width > $i_height) {
            if ($this->_image->getSize()->getWidth() > $this->_width) {
                $this->_image->resize($this->_image->getSize()->widen($this->_width));
            }
        } else {
            if ($this->_image->getSize()->getHeight() > $this->_height) {
                $this->_image->resize($this->_image->getSize()->heighten($this->_height));
            }
        }

        $image = (new Imagine)->create($target, $color);
        $image->paste($this->_image, $cropBy);

        $this->_image = $image;
    }
}