<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 25.12.2018
 * Time: 16:17
 */

namespace app\models\photo;


use app\models\photo\AbstractSize;
use app\models\photo\Photo;

/**
 * Class SizeFactory
 * @package app\models\photo\size
 */
class SizeFactory
{
    const SIZE_ABBR_ORIGINAL = 'O';
    const SIZE_ABBR_THUMBNAIL = 'T';
    const SIZE_ABBR_SMALL = 'S';
    const SIZE_ABBR_MEDIUM = 'M';
    const SIZE_ABBR_LARGE = 'L';

    const SIZE_ORIGINAL = 'Original';
    const SIZE_THUMBNAIL = 'Thumbnail';
    const SIZE_SMALL = 'Small';
    const SIZE_MEDIUM = 'Medium';
    const SIZE_LARGE = 'Large';

    /**
     * @param $size
     * @return AbstractSize
     */
    public static function factory($size)
    {
        $classname = "app\\models\\photo\\size\\" . ucfirst($size);
        return new $classname();
    }

    /**
     * @param $abbr
     * @return AbstractSize
     */
    public static function fromAbbrv($abbr)
    {
        $size = self::_translateAbbrToSize($abbr);
        return self::factory($size);
    }

    /**
     * @param $abbr
     * @return string
     */
    protected static function _translateAbbrToSize($abbr)
    {
        // todo nemely by byt v teto tride?...
        $sizes = Photo::getSizes();
        return $sizes[$abbr];
    }
}