<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 25.12.2018
 * Time: 17:40
 */

namespace app\models\photo;

/**
 * Interface PhotoResizable
 * @package app\models\photo
 */
interface PhotoResizable
{
    /**
     * @return mixed
     */
    public function resize();
}