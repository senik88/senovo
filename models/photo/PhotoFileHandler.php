<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 25.12.2018
 * Time: 19:59
 */

namespace app\models\photo;


use app\models\File;
use Imagine\Image\ImageInterface;

/**
 * Class PhotoFileHandler
 * @package app\models\photo
 */
class PhotoFileHandler
{
    /** @var  File */
    protected $_file;
    /** @var  ImageInterface */
    protected $_image;

    /**
     * @return File
     */
    public function getFile()
    {
        return $this->_file;
    }

    /**
     * @param File $file
     */
    public function setFile(& $file)
    {
        $this->_file = $file;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->_image;
    }

    /**
     * @param mixed $image
     */
    public function setImage(& $image)
    {
        $this->_image = $image;
    }

    /**
     *
     */
    public function handle()
    {
        $name = File::getNewPkey();

        $path = $this->_file->getAbsolutePath() . $this->_file->path;

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        if (!is_writable($path)) {
            chmod($path, 0777);
        }

        $savePath = $path . DIRECTORY_SEPARATOR . $name;

        $this->_image->save($savePath, ['format' => substr($this->_file->name, strrpos($this->_file->name, '.')+1)]);

        $this->_file->file_pk = $name;
        $this->_file->size = filesize($savePath);
        $this->_file->hash = md5($savePath . '|' . time());
    }
}