<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 06.01.2016
 * Time: 18:33
 */

namespace app\models;


use yii\base\Model;

/**
 * Class Reference
 * @package app\models
 */
class Reference extends Model
{
    /**
     * @var
     */
    public $reference_pk;

    /**
     * @var
     */
    public $type;

    /**
     * @var
     */
    public $date;

    /**
     * @var
     */
    public $description;

    /**
     * @var
     */
    public $icon;

    /**
     * @var
     */
    public $hasMoreReferences;

    /**
     * @param null $offset
     * @param int $limit
     * @return array
     */
    public function search($offset = null, $limit = 3)
    {
        $mHomepage = new Homepage();

        $data = $mHomepage->getReferences();

        $keys = array_keys($data);

        $this->hasMoreReferences = true;

        $end = $offset + $limit;

        if ($end >= count($keys)) {
            $end = count($keys);
            $this->hasMoreReferences = false;
        }

        $return = [];
        for ($i = $offset; $i < $end; $i++) {
            $return[] = $data[$keys[$i]];
        }

        return $return;
    }
}