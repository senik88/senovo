<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 02.04.2018
 * Time: 18:59
 */

namespace app\models\article;


use app\components\senovo\ItemAliasTrait;
use app\components\senovo\ActiveRecord;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;

/**
 * Class Article
 * @package app\models\article
 *
 * @property integer $article_pk
 * @property string $article_id
 * @property integer $article_pos
 * @property string $picture
 * @property string $picture_pos
 * @property string $title
 * @property string $content
 * @property string $show_up
 * @property string $published
 */
class Article extends ActiveRecord
{

    use ItemAliasTrait;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['article_id', 'article_pos', 'title', 'content', 'picture', 'picture_pos', 'show_up', 'published'], 'safe']
        ];
    }

    /**
     * @return ActiveDataProvider
     */
    public function getDataProvider()
    {
        return new ActiveDataProvider([
            'query' => self::find(),
            'sort' => [
                'attributes' => ['article_id', 'title', 'published', 'article_pos'],
                'defaultOrder' => ['article_pos' => SORT_ASC, 'published' => SORT_ASC]
            ],
            'pagination' => false
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'show_up' => 'Up btn',
            'article_pos' => 'Position'
        ];
    }

    /**
     * @return array
     */
    public function getColumns()
    {
        return [
            'article_id', 'title',
            'content' => [
                'attribute' => 'content',
                'value' => function($model) {
                    return mb_substr(strip_tags($model['content']), 0, 128) . '...';
                }
            ],
            'picture', 'picture_pos', 'show_up',
            'article_pos' => [
                'format' => 'raw',
                'attribute' => 'article_pos',
                'value' => function($model) {
                    return '<input type="number" min="1" step="1" name="Article[' . $model->article_pk . '][article_pos]" value="' . $model->article_pos . '" />';
                }
            ],
            'published' => [
                'attribute' => 'published',
                'format' => 'datetime'
            ],
            'actions' => [
                'class' => ActionColumn::className(),
                'header' => 'Actions'
            ]
        ];
    }

    /**
     * @return string
     */
    public static function getPathInRuntime()
    {
        return 'articles';
    }

    /**
     * @return array
     */
    protected static function itemAliasData()
    {
        return [
            'picture_pos' => [
                'LEFT' => 'Left',
                'CENTER' => 'Center',
                'RIGHT' => 'Right',
            ]
        ];
    }
}