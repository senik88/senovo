<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 03.01.2016
 * Time: 21:30
 */

namespace app\models;


use app\models\article\Article;
use app\models\reference\Reference;
use Yii;
use yii\base\Model;
use yii\bootstrap\Html;

/**
 * Class Homepage
 * @package app\models
 */
class Homepage extends Model
{
    /** @var self */
    protected static $_instance;

    /**
     * @return Homepage
     */
    public static function getInstance()
    {
        if (self::$_instance == null) {
            self::$_instance = new self;
        }

        return self::$_instance;
    }

    /**
     * @return array
     */
    public static function getMenu()
    {
        $menu = [];
        $menu[] = ['label' => 'Úvod', 'url' => Yii::$app->homeUrl, 'options' => ['class' => 'menu-link']];

        foreach (self::getInstance()->getArticles() as $mArticle) {
            $menu[] = ['label' => $mArticle->title, 'url' => ['/site/' . $mArticle->article_id], 'options' => ['class' => 'menu-link', 'data-anchor' => $mArticle->article_id]];
        }

        $menu[] = ['label' => 'Reference', 'url' => ['/site/reference'], 'options' => ['class' => 'menu-link', 'data-anchor' => 'reference']];
        $menu[] = ['label' => 'Kontakt', 'url' => ['/site/kontakt'], 'options' => ['class' => 'menu-link', 'data-anchor' => 'kontakt']];

        return $menu;
    }

    /**
     *
     */
    public function getBlocks()
    {
        return [

        ];
    }

    /**
     * @return Article[]
     */
    public function getArticles()
    {
        return Article::find()->where('published is not null and now() >= published')->orderBy(['article_pos' => SORT_ASC, 'published' => SORT_ASC])->all();
    }

    /**
     * @return array
     */
    public function getCarousel()
    {
        return $this->getCarouselData();
    }

    /**
     * @param null $limit
     * @param null $offset
     * @return reference\Reference[]
     */
    public function getReferences($limit = null, $offset = null)
    {
        $query = Reference::find()->where('published is not null and now() >= published');
        $query->orderBy(['ref_date' => SORT_DESC]); // todo orly?

        if ($limit !== null) {
            $query->limit($limit);
        }

        if ($offset !== null) {
            $query->offset($offset);
        }

        return $query->all();
    }

    /**
     * Testovaci data pro carousel taham z boxu s textem
     * @return array
     */
    protected function getCarouselData()
    {
        $carousel = [];

        foreach ($this->getArticles() as $mArticle) {
            $carousel[] = Html::tag('h2', $mArticle->title);
        }

        return $carousel;
    }
}