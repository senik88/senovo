<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 01.04.2018
 * Time: 20:41
 */

namespace app\models\reference;


use app\components\senovo\ActiveRecord;
use app\components\senovo\SqlDataProvider;
use app\models\Gallery;
use app\models\reference\type\AbstractType;
use app\models\reference\type\TypeFactory;
use yii\bootstrap\Html;
use yii\grid\ActionColumn;

/**
 * Class Reference
 * @package app\models\reference
 *
 * @property integer $reference_pk
 * @property string $type
 * @property string $ref_date
 * @property string $content
 * @property string $link
 * @property string $published
 * @property integer photogallery_pk
 */
class Reference extends ActiveRecord
{
    /**
     * @var AbstractType
     */
    protected $_type;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['reference_pk', 'type', 'ref_date', 'content', 'link', 'published', 'photogallery_pk'], 'safe']
        ];
    }

    /**
     * @return array
     */
    public function getColumns()
    {
        return [
            'type',
            'ref_date' => [
                'attribute' => 'ref_date',
                'format' => 'date'
            ],
            'content' => [
                'attribute' => 'content',
                'value' => function($model) {
                    return mb_substr(strip_tags($model['content']), 0, 128) . '...';
                }
            ],
            'published' => [
                'attribute' => 'published',
                'format' => 'datetime'
            ],
            'actions' => [
                'class' => ActionColumn::className(),
                'header' => 'Actions',
                'template' => '{view} {update} {delete}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
//                        \Kint::dump($url, $model, $key);
                        return Html::a(
                            Html::icon('eye-open'),
                            ['/cms/preview/reference', 'id' => $model['reference_pk']],
                            [
                                'title' => 'Preview',
//                                'data-toggle' => 'modal',
                                'data-target' => '#reference-modal',
//                                'data-url'
                                'class' => 'link-reference-preview'
                            ]
                        );
                    }
                ]
            ]
        ];
    }

    /**
     *
     */
    public function getDataProvider()
    {
        $sql = $this->getSql();
        $params = $where = [];

        $val = $this->_filter->type;
        if ($val != null) {
            $params[':type'] = $val;
            $where[] = 'type = :type';
        }

        $val = $this->_filter->content; // todo doladit fulltext search
        if ($val != null) {
            $params[':content'] = $val;
            $where[] = 'to_tsvector(content) @@ to_tsquery(:content)';
        }

        $from = $this->_filter->date_from;
        $to  = $this->_filter->date_to;
        if ($from != null && $to != null) {
            $params[':date_from'] = $from;
            $params[':date_to'] = $to;
            $where[] = '(published BETWEEN :date_from AND :date_to)';
        }

        $from = $this->_filter->published_from;
        $to  = $this->_filter->published_to;
        if ($from != null && $to != null) {
            $params[':published_from'] = $from;
            $params[':published_to'] = $to;
            $where[] = '(published BETWEEN :published_from AND :published_to)';
        }

        if (!empty($where)) {
            $sql .= "\nWHERE " . implode("\nAND ", $where);
        }

        return new SqlDataProvider([
            'sql' => $sql,
            'params' => $params,
            'key' => 'reference_pk',
            'sort' => [
                'attributes' => ['type', 'ref_date', 'published'],
                'defaultOrder' => ['ref_date' => SORT_DESC]
            ]
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGallery()
    {
        return $this->hasOne(Gallery::class, ['photogallery_pk' => 'photogallery_pk']);
    }

    /**
     * @return bool
     */
    public function hasGallery(): bool
    {
        return $this->gallery !== null;
    }

    /**
     * @return AbstractType
     * @throws \Exception
     */
    public function getType()
    {
        if ($this->_type == null) {
            $this->_type = TypeFactory::factory($this->type);
        }

        return $this->_type;
    }

    /**
     * @return string
     */
    private function getSql()
    {
        return "
            SELECT * FROM reference
        ";
    }
}