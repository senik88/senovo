<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 01.04.2018
 * Time: 20:42
 */

namespace app\models\reference\type;

/**
 * Class TypeFactory
 * @package app\models\reference\type
 */
class TypeFactory
{
    /**
     * @var array
     */
    private static $_mapping = [
        AbstractType::ID_PC => 'Computer',
        AbstractType::ID_WEB => 'Web',
        AbstractType::ID_OTHER => 'Other'
    ];

    /**
     * @param $id
     * @return AbstractType
     * @throws \Exception
     */
    public static function factory($id)
    {
        if (!isset(self::$_mapping[$id])) {
            throw new \Exception("Reference type with ID ($id) does not exist!");
        }

        $className = "app\\models\\reference\\type\\" . self::$_mapping[$id];

        return new $className;
    }

    /**
     * @return array
     */
    public static function getMapping()
    {
        return self::$_mapping;
    }
}