<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 01.04.2018
 * Time: 20:51
 */

namespace app\models\reference\type;

/**
 * Class AbstractType
 * @package app\models\reference\type
 */
abstract class AbstractType
{
    const ID_WEB = 'WEB';
    const ID_PC = 'PC';
    const ID_OTHER = 'OTHER';

    /**
     * @var
     */
    public $id;

    /**
     * @var
     */
    public $icon;

    /**
     *
     */
    public function getIconFullPath()
    {
        return \Yii::$app->request->baseUrl . '/images/' . $this->icon;
    }
}