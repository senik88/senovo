<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 01.04.2018
 * Time: 20:41
 */

namespace app\models\reference\type;

/**
 * Class Web
 * @package app\models\reference\type
 */
class Web extends AbstractType
{

    public $id = AbstractType::ID_WEB;

    public $icon = 'browser.png';

}