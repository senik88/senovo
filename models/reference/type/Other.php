<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 01.04.2018
 * Time: 20:43
 */

namespace app\models\reference\type;

/**
 * Class Computer
 * @package app\models\reference\type
 */
class Other extends AbstractType {

    public $id = AbstractType::ID_OTHER;

    public $icon = '/icons/document.png';

}