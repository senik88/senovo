CONTAINER=senovo_cms

export

upf:
	docker-compose -f ./docker/docker-compose.yml up

up:
	docker-compose -f ./docker/docker-compose.yml up -d

down:
	docker-compose -f ./docker/docker-compose.yml down

build:
	docker-compose -f ./docker/docker-compose.yml build

bash-root:
	docker exec -it ${CONTAINER} /bin/bash
