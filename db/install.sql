BEGIN;

-- schema
\i ./create/lite.sql

-- procedury

-- data
\i ./init/data.sql

ALTER SEQUENCE file_file_pk_seq RESTART WITH 100000; -- restart, pokud uz je nalita DB