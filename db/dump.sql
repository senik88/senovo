--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: article; Type: TABLE; Schema: public; Owner: senik; Tablespace: 
--

CREATE TABLE article (
    article_pk integer NOT NULL,
    article_id text NOT NULL,
    title text NOT NULL,
    content text,
    picture text,
    picture_pos text NOT NULL,
    show_up boolean DEFAULT true NOT NULL,
    article_pos bigint,
    published timestamp with time zone,
    CONSTRAINT picture_check_position CHECK ((picture_pos = ANY (ARRAY['CENTER'::text, 'LEFT'::text, 'RIGHT'::text])))
);


ALTER TABLE article OWNER TO senik;

--
-- Name: article_article_pk_seq; Type: SEQUENCE; Schema: public; Owner: senik
--

CREATE SEQUENCE article_article_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE article_article_pk_seq OWNER TO senik;

--
-- Name: article_article_pk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: senik
--

ALTER SEQUENCE article_article_pk_seq OWNED BY article.article_pk;


--
-- Name: file; Type: TABLE; Schema: public; Owner: senik; Tablespace: 
--

CREATE TABLE file (
    file_pk integer NOT NULL,
    size bigint NOT NULL,
    hash text NOT NULL,
    name text NOT NULL,
    path text NOT NULL,
    title text,
    uploaded timestamp with time zone NOT NULL,
    mimetype text NOT NULL
);


ALTER TABLE file OWNER TO senik;

--
-- Name: file_file_pk_seq; Type: SEQUENCE; Schema: public; Owner: senik
--

CREATE SEQUENCE file_file_pk_seq
    START WITH 100000
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE file_file_pk_seq OWNER TO senik;

--
-- Name: file_file_pk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: senik
--

ALTER SEQUENCE file_file_pk_seq OWNED BY file.file_pk;


--
-- Name: menu_item; Type: TABLE; Schema: public; Owner: senik; Tablespace: 
--

CREATE TABLE menu_item (
    menu_item_pk integer NOT NULL,
    menu_item_id text NOT NULL,
    title text NOT NULL,
    show boolean DEFAULT true NOT NULL,
    "position" smallint NOT NULL,
    article_pk integer
);


ALTER TABLE menu_item OWNER TO senik;

--
-- Name: menu_item_menu_item_pk_seq; Type: SEQUENCE; Schema: public; Owner: senik
--

CREATE SEQUENCE menu_item_menu_item_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE menu_item_menu_item_pk_seq OWNER TO senik;

--
-- Name: menu_item_menu_item_pk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: senik
--

ALTER SEQUENCE menu_item_menu_item_pk_seq OWNED BY menu_item.menu_item_pk;


--
-- Name: reference; Type: TABLE; Schema: public; Owner: senik; Tablespace: 
--

CREATE TABLE reference (
    reference_pk integer NOT NULL,
    content text NOT NULL,
    type text NOT NULL,
    link text,
    ref_date date NOT NULL,
    published timestamp with time zone,
    CONSTRAINT check_reference_type CHECK ((type = ANY (ARRAY['WEB'::text, 'PC'::text, 'OTHER'::text])))
);


ALTER TABLE reference OWNER TO senik;

--
-- Name: reference_reference_pk_seq; Type: SEQUENCE; Schema: public; Owner: senik
--

CREATE SEQUENCE reference_reference_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE reference_reference_pk_seq OWNER TO senik;

--
-- Name: reference_reference_pk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: senik
--

ALTER SEQUENCE reference_reference_pk_seq OWNED BY reference.reference_pk;


--
-- Name: article_pk; Type: DEFAULT; Schema: public; Owner: senik
--

ALTER TABLE ONLY article ALTER COLUMN article_pk SET DEFAULT nextval('article_article_pk_seq'::regclass);


--
-- Name: file_pk; Type: DEFAULT; Schema: public; Owner: senik
--

ALTER TABLE ONLY file ALTER COLUMN file_pk SET DEFAULT nextval('file_file_pk_seq'::regclass);


--
-- Name: menu_item_pk; Type: DEFAULT; Schema: public; Owner: senik
--

ALTER TABLE ONLY menu_item ALTER COLUMN menu_item_pk SET DEFAULT nextval('menu_item_menu_item_pk_seq'::regclass);


--
-- Name: reference_pk; Type: DEFAULT; Schema: public; Owner: senik
--

ALTER TABLE ONLY reference ALTER COLUMN reference_pk SET DEFAULT nextval('reference_reference_pk_seq'::regclass);


--
-- Data for Name: article; Type: TABLE DATA; Schema: public; Owner: senik
--

COPY article (article_pk, article_id, title, content, picture, picture_pos, show_up, article_pos, published) FROM stdin;
1	sestavy	PC sestavy	<p>Nové PC sestavím, nainstaluji legální OS dle Vašeho výběru, veškeré ovladače a aktualizace, doplním antivirovým programem, sadou nejdůležitějšího freeware softwaru a nakonec řádně otestuji. Připravené PC přivezu k zákazníkovi, zapojím a předvedu. HW vybírám vždy na míru zákazníkovi.</p>	023912bc5be42c89f268c3f047f74646	LEFT	t	3	2018-04-26 20:58:17.085005+02
2	poradenstvi	HW & SW & Sítě	<p>Máte pomalý počítač nebo notebook? Hučí a přehřívá se? Nic z toho není pro mne problém. Celý počítač očistím od prachu a vyměním teplovodivou pastu, která může být po letech provozu vyschlá. Dále pomohu s výběrem komponent pro upgrade stávající sestavy a komponenty nainstaluji. Provádím také antivirové a antispyware kontroly a čištění PC.</p>	f714287ad91ed111018a45e53faf9b38	RIGHT	t	2	2018-04-26 20:58:17.085005+02
3	weby	Tvorba webů	<p>Zabývám se tvorbou jednoduchých stránek, webových aplikací i komplexních informačních systémů. Webové stránky kóduji v xHTML a za pomoci kaskádových stylů (CSS), pro složitější aplikace využívám Yii framework a PostgreSQL databáze. Dále zajistím i věci s webovými stránkami související jako hosting a e-mailovou schránku.</p>	e282e456b299cb84962d37de5ab105fa	LEFT	t	1	2018-04-26 20:58:17.085005+02
\.


--
-- Name: article_article_pk_seq; Type: SEQUENCE SET; Schema: public; Owner: senik
--

SELECT pg_catalog.setval('article_article_pk_seq', 3, true);


--
-- Data for Name: file; Type: TABLE DATA; Schema: public; Owner: senik
--

COPY file (file_pk, size, hash, name, path, title, uploaded, mimetype) FROM stdin;
1	335345	0dad517d3d0802cd631d00316584083d	SDS-IT Consulting-1.jpg	articles	\N	2018-04-26 21:21:41+02	image/jpeg
2	106648	474b211aa447cb6b3e0fddfd67265f90	hardware-software.jpg	articles	\N	2018-04-26 21:27:03+02	image/jpeg
3	293738	688c2d442be74ceec2d34d22eefebc9d	photodune-2392898-server-m2.jpg	articles	\N	2018-04-27 20:53:31+02	image/jpeg
8	197499	f714287ad91ed111018a45e53faf9b38	hw-sw-site.jpg	articles	\N	2018-04-28 14:06:50+02	image/jpeg
9	178992	6e760d2b7c89821e19f64d2dcd7288ed	monitor-flat.png	articles	\N	2018-04-28 14:07:20+02	image/png
12	145681	e282e456b299cb84962d37de5ab105fa	source-code.jpg	articles	\N	2018-04-28 14:11:21+02	image/jpeg
13	317231	023912bc5be42c89f268c3f047f74646	computer2.jpg	articles	\N	2018-04-28 14:12:21+02	image/jpeg
\.


--
-- Name: file_file_pk_seq; Type: SEQUENCE SET; Schema: public; Owner: senik
--

SELECT pg_catalog.setval('file_file_pk_seq', 15, true);


--
-- Data for Name: menu_item; Type: TABLE DATA; Schema: public; Owner: senik
--

COPY menu_item (menu_item_pk, menu_item_id, title, show, "position", article_pk) FROM stdin;
\.


--
-- Name: menu_item_menu_item_pk_seq; Type: SEQUENCE SET; Schema: public; Owner: senik
--

SELECT pg_catalog.setval('menu_item_menu_item_pk_seq', 1, false);


--
-- Data for Name: reference; Type: TABLE DATA; Schema: public; Owner: senik
--

COPY reference (reference_pk, content, type, link, ref_date, published) FROM stdin;
1	<p>Tyto jednoduché stránky jsou mou prací. Stránky slouží jako podklad pro vývoj CMS, který bude sloužit pro tvorbu jednoduchých jednostránkových prezentací. </p> <p> V tuto chvíli je CMS teprve ve fázi návrhu. Mám plánu jej také využít jako modul v rámci vlastního řešení informačního systému. </p>	WEB	\N	2016-01-16	2018-04-26 20:58:17.085005+02
2	<p>Webová aplikace pro partu nadšenců na motorkách ve fázi vývoje. Aplikace slouží k vyvěšování termínů společného ježdění na okruhu ve Vysokém Mýtě. Umožňuje registraci uživatelů, správu termínů a plateb, odběr informací o nových termínech, přihlašování a odhlašování na termíny. </p> <p> Součástí aplikace je napojení na externí mailovou službu, konzolová část aplikace volaná CRONem a stahování počasí přes externí API. </p>	WEB	http://vmyto.senovo.cz	2015-12-31	2018-04-26 20:58:17.085005+02
3	<p>Individuální informační systém pro firmu VoPlasTo, která se zabývá zakázkovým tiskem na 3D tiskárnách, jejich výrobou a prodejem. IS umožňuje správu zákazníků, evidenci objednávek a faktur, sledování skladových zásob tiskového materiálu a funguje také jako úložiště pro 3D modely. </p> <p> Systém je průběžně vyvíjen a spravován, do budoucna je uvažováno napojení na eshop, který bude postaven na aktuálně vyvíjeném řešení. </p>	WEB	http://is.voplasto.cz	2015-07-31	2018-04-26 20:58:17.085005+02
4	<p> <b>Zadání</b> - Tiché PC primárně určené k přehrávání filmů na LCD televizi v obývacím pokoji a občasné hraní her. Hlavním požadavkem bylo ticho, ale zůstat v rozumné cenové hladině – cca 16 000 Kč za HW bez sestavení. V sestavě není zahrnut operační systém. </p> <p> <b>Sestava</b> – AMD Phenom II X4 955 BE | Gigabyte GA-790XT-USB3 | Kingston 4GB 1333MHz | Gigabyte Radeon HD 5770 1GB | Seagate Barracuda 7200.12 3.5″ 1TB | Seasonic M12II-520 520W | Scythe Ninja 3 | Samsung SH-S223C | Fractal Design Define R3 Black Pearl | <b>Cena: 17 250 Kč</b> </p>	PC	\N	2011-01-24	2018-04-26 20:58:17.085005+02
5	<p> <b>Zadání</b> – Internet, prohlížení fotek, ne příliš náročné hry pro syna. Počítá se i s digitální kamerou, takže jednoduché zpracování a prohlížení vlastní tvorby. </p> <p> <b>Sestava</b> – AMD Athlon II X3 440 | MSI 770-G45 | Kingston 2GB DDR3 1333MHz | Sapphire ATI Radeon HD 5670 512MB DDR5 | Seagate Barracuda 7200.12 500GB | Seasonic SS-400ET 400W | Cooler Master Hyper TX3 | LG GH22LS | Cooler Master Elite 335 | Windows 7 Home Premium CZ 64bit OEM | BenQ 23″ G2320HDB | Logitech Deluxe 250 Keyboard Black | <b>Cena: 18 250 Kč</b> </p>	PC	\N	2010-07-05	2018-04-26 20:58:17.085005+02
6	<p><strong>Zadání</strong> - PC na hraní her ve FulHD rozlišení, pouze bedna + OS, monitor a další periferie budou použity ze staré sestavy.\r\n</p><p><strong>Sestava</strong> – AMD Ryzen 5 1500X | GIGABYTE AM4 AB350M-Gaming 3 | Crucial Ballistix Sport LT Red 8GB | Sapphire Radeon NITRO+ RX 480 4GB | ADATA Premier Pro SP920 256GB | Seagate BarraCuda 1TB | Seasonic SS-500ET-F3 | SilentiumPC Spartan 3 Pro | ASUS DRW-24D5MT | CoolerMaster N200 | <strong>Cena: 26 000 Kč</strong>\r\n</p>	PC		2017-04-12	2018-11-17 00:00:00+01
7	<p><strong>Zadání</strong> - PC na hraní her ve FulHD rozlišení, pouze bedna + OS, monitor a další periferie budou použity ze staré sestavy. Cena okolo 18 tisíc Kč.</p><p><strong>Sestava</strong> – AMD Ryzen 5 1400 | ASUS Prime B450M-K | Crucial Ballistix Sport AT 8GB 3000 MHz | Sapphire Radeon NITRO+ RX 580 OC 4GB | Crucial MX500 250GB M.2 | Seagate BarraCuda 1TB | Seasonic SS-500ET-T3 | SilentiumPC Spartan 3 LT | ASUS DRW-24D5MT | CoolerMaster MasterBox Lite 3 | Arctic Fan F12 Pro | <strong>Cena: 18 560 Kč</strong></p>	PC		2018-11-12	2018-12-13 17:00:00+01
\.


--
-- Name: reference_reference_pk_seq; Type: SEQUENCE SET; Schema: public; Owner: senik
--

SELECT pg_catalog.setval('reference_reference_pk_seq', 7, true);


--
-- Name: Key1; Type: CONSTRAINT; Schema: public; Owner: senik; Tablespace: 
--

ALTER TABLE ONLY article
    ADD CONSTRAINT "Key1" PRIMARY KEY (article_pk);


--
-- Name: Key2; Type: CONSTRAINT; Schema: public; Owner: senik; Tablespace: 
--

ALTER TABLE ONLY menu_item
    ADD CONSTRAINT "Key2" PRIMARY KEY (menu_item_pk);


--
-- Name: Key3; Type: CONSTRAINT; Schema: public; Owner: senik; Tablespace: 
--

ALTER TABLE ONLY reference
    ADD CONSTRAINT "Key3" PRIMARY KEY (reference_pk);


--
-- Name: Key4; Type: CONSTRAINT; Schema: public; Owner: senik; Tablespace: 
--

ALTER TABLE ONLY file
    ADD CONSTRAINT "Key4" PRIMARY KEY (file_pk);


--
-- Name: article_id; Type: CONSTRAINT; Schema: public; Owner: senik; Tablespace: 
--

ALTER TABLE ONLY article
    ADD CONSTRAINT article_id UNIQUE (article_id);


--
-- Name: hash; Type: CONSTRAINT; Schema: public; Owner: senik; Tablespace: 
--

ALTER TABLE ONLY file
    ADD CONSTRAINT hash UNIQUE (hash);


--
-- Name: menu_item_id; Type: CONSTRAINT; Schema: public; Owner: senik; Tablespace: 
--

ALTER TABLE ONLY menu_item
    ADD CONSTRAINT menu_item_id UNIQUE (menu_item_id);


--
-- Name: position; Type: CONSTRAINT; Schema: public; Owner: senik; Tablespace: 
--

ALTER TABLE ONLY menu_item
    ADD CONSTRAINT "position" UNIQUE ("position");


--
-- Name: IX_Relationship1; Type: INDEX; Schema: public; Owner: senik; Tablespace: 
--

CREATE INDEX "IX_Relationship1" ON menu_item USING btree (article_pk);


--
-- Name: IX_Relationship2; Type: INDEX; Schema: public; Owner: senik; Tablespace: 
--

CREATE INDEX "IX_Relationship2" ON article USING btree (picture);


--
-- Name: Relationship1; Type: FK CONSTRAINT; Schema: public; Owner: senik
--

ALTER TABLE ONLY menu_item
    ADD CONSTRAINT "Relationship1" FOREIGN KEY (article_pk) REFERENCES article(article_pk);


--
-- Name: Relationship2; Type: FK CONSTRAINT; Schema: public; Owner: senik
--

ALTER TABLE ONLY article
    ADD CONSTRAINT "Relationship2" FOREIGN KEY (picture) REFERENCES file(hash) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

