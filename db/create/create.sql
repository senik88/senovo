﻿/*
Created: 21.11.2016
Modified: 13.01.2017
Project: CMS
Model: cms
Company: Senovo.cz
Author: Lukáš Senohrábek
Database: PostgreSQL 9.4
*/


-- Create tables section -------------------------------------------------

-- Table page

CREATE TABLE "page"(
 "page_pk" Serial NOT NULL,
 "page_id" Text NOT NULL,
 "title" Text NOT NULL,
 "href" Text
)
;

-- Add keys for table page

ALTER TABLE "page" ADD CONSTRAINT "Key1" PRIMARY KEY ("page_pk")
;

ALTER TABLE "page" ADD CONSTRAINT "page_id" UNIQUE ("page_id")
;

-- Table block

CREATE TABLE "block"(
 "block_pk" Serial NOT NULL,
 "block_id" Text NOT NULL,
 "title" Text NOT NULL,
 "block_order" Integer NOT NULL,
 "page_pk" Integer
)
;

-- Create indexes for table block

CREATE INDEX "ix_block_page" ON "block" ("page_pk")
;

-- Add keys for table block

ALTER TABLE "block" ADD CONSTRAINT "Key2" PRIMARY KEY ("block_pk")
;

ALTER TABLE "block" ADD CONSTRAINT "block_id" UNIQUE ("block_id")
;

-- Table content

CREATE TABLE "content"(
 "content_pk" Serial NOT NULL,
 "content_id" Text NOT NULL,
 "title" Text NOT NULL,
 "block_pk" Integer,
 "content_type_pk" Integer,
 "published" Boolean
)
;

-- Create indexes for table content

CREATE INDEX "ix_content_block_pk" ON "content" ("block_pk")
;

CREATE INDEX "ix_content_content_type_pk" ON "content" ("content_type_pk")
;

-- Add keys for table content

ALTER TABLE "content" ADD CONSTRAINT "Key3" PRIMARY KEY ("content_pk")
;

ALTER TABLE "content" ADD CONSTRAINT "content_id" UNIQUE ("content_id")
;

-- Table field

CREATE TABLE "field"(
 "field_pk" Serial NOT NULL,
 "field_id" Text NOT NULL,
 "title" Text NOT NULL,
 "values" Json,
 "field_order" Integer NOT NULL,
 "content_pk" Integer,
 "field_type_pk" Integer
)
;

-- Create indexes for table field

CREATE INDEX "ix_field_content_pk" ON "field" ("content_pk")
;

CREATE INDEX "ix_field_field_type_pk" ON "field" ("field_type_pk")
;

-- Add keys for table field

ALTER TABLE "field" ADD CONSTRAINT "Key4" PRIMARY KEY ("field_pk")
;

ALTER TABLE "field" ADD CONSTRAINT "field_id" UNIQUE ("field_id")
;

-- Table content_type

CREATE TABLE "content_type"(
 "content_type_pk" Serial NOT NULL,
 "content_type_id" Text NOT NULL,
 "title" Text NOT NULL,
 "layout_pk" Integer
)
;

-- Create indexes for table content_type

CREATE INDEX "ix_content_type_layout_pk" ON "content_type" ("layout_pk")
;

-- Add keys for table content_type

ALTER TABLE "content_type" ADD CONSTRAINT "Key5" PRIMARY KEY ("content_type_pk")
;

ALTER TABLE "content_type" ADD CONSTRAINT "content_type_id" UNIQUE ("content_type_id")
;

-- Table field_type

CREATE TABLE "field_type"(
 "field_type_pk" Serial NOT NULL,
 "field_type_id" Text NOT NULL,
 "type" Text NOT NULL,
 "multiple" Boolean DEFAULT false NOT NULL
)
;

-- Add keys for table field_type

ALTER TABLE "field_type" ADD CONSTRAINT "Key6" PRIMARY KEY ("field_type_pk")
;

ALTER TABLE "field_type" ADD CONSTRAINT "field_type_id" UNIQUE ("field_type_id")
;

-- Table layout

CREATE TABLE "layout"(
 "layout_pk" Serial NOT NULL,
 "layout_id" Text NOT NULL,
 "title" Text NOT NULL,
 "file" Text
)
;

-- Add keys for table layout

ALTER TABLE "layout" ADD CONSTRAINT "Key7" PRIMARY KEY ("layout_pk")
;

ALTER TABLE "layout" ADD CONSTRAINT "layout_id" UNIQUE ("layout_id")
;

-- Table layout_field_type

CREATE TABLE "layout_field_type"(
 "field_type_pk" Integer NOT NULL,
 "layout_pk" Integer NOT NULL,
 "title" Text NOT NULL
)
;

-- Create indexes for table layout_field_type

CREATE INDEX "IX_Relationship7" ON "layout_field_type" ("field_type_pk")
;

CREATE INDEX "IX_Relationship8" ON "layout_field_type" ("layout_pk")
;

-- Add keys for table layout_field_type

ALTER TABLE "layout_field_type" ADD CONSTRAINT "Key8" PRIMARY KEY ("field_type_pk","layout_pk")
;

-- Create relationships section ------------------------------------------------- 

ALTER TABLE "block" ADD CONSTRAINT "block_in_page" FOREIGN KEY ("page_pk") REFERENCES "page" ("page_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "content" ADD CONSTRAINT "content_in_block" FOREIGN KEY ("block_pk") REFERENCES "block" ("block_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "field" ADD CONSTRAINT "content_of_fields" FOREIGN KEY ("content_pk") REFERENCES "content" ("content_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "content" ADD CONSTRAINT "Relationship5" FOREIGN KEY ("content_type_pk") REFERENCES "content_type" ("content_type_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "field" ADD CONSTRAINT "Relationship6" FOREIGN KEY ("field_type_pk") REFERENCES "field_type" ("field_type_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "layout_field_type" ADD CONSTRAINT "Relationship7" FOREIGN KEY ("field_type_pk") REFERENCES "field_type" ("field_type_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "layout_field_type" ADD CONSTRAINT "Relationship8" FOREIGN KEY ("layout_pk") REFERENCES "layout" ("layout_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "content_type" ADD CONSTRAINT "Relationship9" FOREIGN KEY ("layout_pk") REFERENCES "layout" ("layout_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;





