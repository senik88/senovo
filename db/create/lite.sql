﻿/*
Created: 21.11.2016
Modified: 17.12.2018
Project: CMS
Model: cms-lite
Company: Senovo.cz
Author: Lukáš Senohrábek
Database: PostgreSQL 9.4
*/


-- Create tables section -------------------------------------------------

-- Table article

CREATE TABLE "article"(
 "article_pk" Serial NOT NULL,
 "article_id" Text NOT NULL,
 "title" Text NOT NULL,
 "content" Text,
 "picture" Text,
 "picture_pos" Text NOT NULL
        CONSTRAINT "picture_check_position" CHECK (picture_pos IN ('CENTER', 'LEFT', 'RIGHT')),
 "show_up" Boolean DEFAULT TRUE NOT NULL,
 "article_pos" Bigint,
 "published" Timestamp with time zone,
 "photogallery_pk" Integer
)
;

-- Create indexes for table article

CREATE INDEX "IX_Relationship2" ON "article" ("picture")
;

CREATE INDEX "IX_Relationship9" ON "article" ("photogallery_pk")
;

-- Add keys for table article

ALTER TABLE "article" ADD CONSTRAINT "Key1" PRIMARY KEY ("article_pk")
;

ALTER TABLE "article" ADD CONSTRAINT "article_id" UNIQUE ("article_id")
;

-- Table menu_item

CREATE TABLE "menu_item"(
 "menu_item_pk" Serial NOT NULL,
 "menu_item_id" Text NOT NULL,
 "title" Text NOT NULL,
 "show" Boolean DEFAULT TRUE NOT NULL,
 "position" Smallint NOT NULL,
 "article_pk" Integer
)
;

-- Create indexes for table menu_item

CREATE INDEX "IX_Relationship1" ON "menu_item" ("article_pk")
;

-- Add keys for table menu_item

ALTER TABLE "menu_item" ADD CONSTRAINT "Key2" PRIMARY KEY ("menu_item_pk")
;

ALTER TABLE "menu_item" ADD CONSTRAINT "menu_item_id" UNIQUE ("menu_item_id")
;

ALTER TABLE "menu_item" ADD CONSTRAINT "position" UNIQUE ("position")
;

-- Table reference

CREATE TABLE "reference"(
 "reference_pk" Serial NOT NULL,
 "content" Text NOT NULL,
 "type" Text NOT NULL
        CONSTRAINT "check_reference_type" CHECK (type IN ('WEB', 'PC', 'OTHER')),
 "link" Text,
 "ref_date" Date NOT NULL,
 "published" Timestamp with time zone,
 "photogallery_pk" Integer
)
;

-- Create indexes for table reference

CREATE INDEX "IX_Relationship11" ON "reference" ("photogallery_pk")
;

-- Add keys for table reference

ALTER TABLE "reference" ADD CONSTRAINT "Key3" PRIMARY KEY ("reference_pk")
;

-- Table file

CREATE TABLE "file"(
 "file_pk" Serial NOT NULL,
 "size" Bigint NOT NULL,
 "hash" Text NOT NULL,
 "name" Text NOT NULL,
 "path" Text NOT NULL,
 "title" Text,
 "uploaded" Timestamp with time zone NOT NULL,
 "mimetype" Text NOT NULL
)
;

-- Add keys for table file

ALTER TABLE "file" ADD CONSTRAINT "Key4" PRIMARY KEY ("file_pk")
;

ALTER TABLE "file" ADD CONSTRAINT "hash" UNIQUE ("hash")
;

-- Table photogallery

CREATE TABLE "photogallery"(
 "description" Text,
 "published" Timestamp with time zone,
 "photogallery_pk" Serial NOT NULL,
 "photogallery_id" Text NOT NULL,
 "name" Text NOT NULL
)
;

-- Add keys for table photogallery

ALTER TABLE "photogallery" ADD CONSTRAINT "Key5" PRIMARY KEY ("photogallery_pk")
;

-- Table photo

CREATE TABLE "photo"(
 "photo_pk" Serial NOT NULL,
 "file_small_pk" Integer,
 "file_medium_pk" Integer,
 "file_large_pk" Integer,
 "file_original_pk" Integer,
 "file_thumbnail_pk" Integer,
 "photogallery_pk" Integer,
 "name" Text,
 "description" Text,
 "original_width" Integer NOT NULL,
 "original_height" Integer NOT NULL
)
;

-- Create indexes for table photo

CREATE INDEX "IX_Relationship3" ON "photo" ("photogallery_pk")
;

CREATE INDEX "IX_Relationship4" ON "photo" ("file_small_pk")
;

CREATE INDEX "IX_Relationship5" ON "photo" ("file_medium_pk")
;

CREATE INDEX "IX_Relationship6" ON "photo" ("file_large_pk")
;

CREATE INDEX "IX_Relationship7" ON "photo" ("file_original_pk")
;

CREATE INDEX "IX_Relationship8" ON "photo" ("file_thumbnail_pk")
;

-- Add keys for table photo

ALTER TABLE "photo" ADD CONSTRAINT "Key6" PRIMARY KEY ("photo_pk")
;

-- Create relationships section ------------------------------------------------- 

ALTER TABLE "menu_item" ADD CONSTRAINT "rel_article_menu_item" FOREIGN KEY ("article_pk") REFERENCES "article" ("article_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "article" ADD CONSTRAINT "Relationship2" FOREIGN KEY ("picture") REFERENCES "file" ("hash") ON DELETE RESTRICT ON UPDATE RESTRICT
;

ALTER TABLE "photo" ADD CONSTRAINT "Relationship3" FOREIGN KEY ("photogallery_pk") REFERENCES "photogallery" ("photogallery_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "photo" ADD CONSTRAINT "Relationship4" FOREIGN KEY ("file_small_pk") REFERENCES "file" ("file_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "photo" ADD CONSTRAINT "Relationship5" FOREIGN KEY ("file_medium_pk") REFERENCES "file" ("file_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "photo" ADD CONSTRAINT "Relationship6" FOREIGN KEY ("file_large_pk") REFERENCES "file" ("file_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "photo" ADD CONSTRAINT "Relationship7" FOREIGN KEY ("file_original_pk") REFERENCES "file" ("file_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "photo" ADD CONSTRAINT "Relationship8" FOREIGN KEY ("file_thumbnail_pk") REFERENCES "file" ("file_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "article" ADD CONSTRAINT "Relationship9" FOREIGN KEY ("photogallery_pk") REFERENCES "photogallery" ("photogallery_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "reference" ADD CONSTRAINT "Relationship11" FOREIGN KEY ("photogallery_pk") REFERENCES "photogallery" ("photogallery_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;





