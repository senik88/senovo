Senovo.cz web page
==================

"Firemní" prezentace, jednoduchý one-page web.

Web slouží jako podklad pro jednoduché CMS - bude tam editovatelný vlastně jen menu, carousel a jednotlivé články.
Možná časem přidám další sekce webu.

Ve footeru budou odkazy jako v menu, adresa firmy, kontaktní formulář a mapa sídla.

Časem bych chtěl, aby šly vytvářet vlastní šablony.

Icons proudly provided by:
https://www.iconfinder.com/iconsets/graficheria
https://www.iconfinder.com/iconsets/ballicons-free
https://www.iconfinder.com/iconsets/flat-designed-circle-icon-2

https://github.com/luyadev

github key
19926e66dc7917abde93b7b15483b11a608f1754

Nápady
======

Api
---

Rest api, CMS bude podporovat správu obsahu přes API
- správa content-typů, jejich definice
- editace obsahu
- obsah bude možné posílat i emailem?


Backend
-------

Klasický backend
- nejprve naprogramovat to - co nejjednodušší
- na základě backendu pak obšlehnout API



Původní obsah webu:
===================

Homepage:
---------

Počítačové sestavy, HW/SW poradenství a webdesign
Jsem student Univerzity Hradec Králové, obor Aplikovaná informatika a zabývám se v první řadě stavbou počítačových sestav na zakázku, přesně na míru zákazníkovi a jeho požadavkům. Mimo to poskytuji poradenství v oblasti IT, správu hardwaru a softwaru, provádím upgrady stávajích sestav. Samozřejmostí je technická podpora a záruka dva roky na mnou vytvořené počítačové sestavy.

Nové PC sestavím, nainstaluji legální (s PC zakoupená nebo zákazníkem dodaná OEM/retail licence) Windows a veškeré ovladače a aktualizace, doplním antivirovým programem, sadou nejdůležitějšího freeware softwaru a nakonec řádně otestuji. Takovéto PC přivezu k zákazníkovi, zapojím a předvedu.

Dále se zabývám tvorbou efektních webových stránek pro každého, kdo ví, že webová prezentace je v dnešní době důležitou součástí firemní identity.

Webové stránky kóduji v xHTML a za pomoci kaskádových stylů (CSS). Dále zajistím i věci s webovými stránkami související jako hosting a e-mailovou schránku, v případě zájmu navrhnu logo.

Jaké služby nabízím?
Opravy PC u Vás doma nebo kanceláři
Sestavení PC na míru zákazníkovi, dodání do 5 pracovních dní
PC přivezu, zapojím a předvedu
Technická podpora na mé sestavy, záruka 2 roky
Upgrady stávajících sestav, čistění PC od prachu
Obnova ztracených dat z HDD, antirové a antispyware kontroly
Vytvoření kompletních webových stránek
Jejich grafický návrh, konzultace a konečné kódování do xHTML a CSS

Ceník:
------

Ceny našich služeb
Stejně tak jako se liší jednotlivé webové stránky mezi sebou, liší se i jejich cena. Každá webová prezentace je individuální záležitostí a její cena se od toho odvíjí, vše je tedy zálěžitostí vzájemné domluvy. A protože jsem student a tuto práci dělám proto, že mě baví, nemám v úmyslu Vás sedřít z kůže.

Naproti tvorbě webu je stavba PC relativně jednoduchou záležitostí a tak je cena za stavbu jednotná. Podmínkou je pouze složení minimálně 50% zálohy z celkové ceny PC předem. U staveb přesahujících celkovou částku 30 000,- Kč bude záležet na individuální domluvě.

Náplň práce – PC sestavy	Cena
Kompletni sestavení PC – co vše je v ceně?	1000 Kč
Servisní zásah u zákazníka – cena za každou započatou hodinu	300 Kč
Přeinstalování Windows – zahrnuje zálohu dat, reinstalaci a obnovení zálohy	700 Kč
Náplň práce – Weby	Cena
Webová vizitka o rozsahu 1-2 stránky, statické	od 1000 Kč
Webová prezentace o rozsahu cca 5 stránek, dynamické	od 2500 Kč
Webový portál o rozsahu cca 10 stránek a více, dynamické, redakční systém	od 4500 Kč
Registrace hostingu a domény	250 Kč
Grafický návrh webu	od 1500 Kč
Návrh loga	od 500 Kč

Sestavy:
--------

Lukáš Senohrábek	| 24.1.2011
Zadání - Tiché PC primárně určené k přehrávání filmů na LCD televizi v obývacím pokoji a občasné hraní her. Hlavním požadavkem bylo ticho, ale zůstat v rozumné cenové hladině – cca 16 000 Kč za HW bez sestavení. V sestavě není zahrnut operační systém.
Sestava – AMD Phenom II X4 955 BE | Gigabyte GA-790XT-USB3 | Kingston 4GB 1333MHz | Gigabyte Radeon HD 5770 1GB | Seagate Barracuda 7200.12 3.5″ 1TB | Seasonic M12II-520 520W | Scythe Ninja 3 | Samsung SH-S223C | Fractal Design Define R3 Black Pearl | Cena: 17 250 Kč

Lukáš Senohrábek	| 5.7.2010
Zadání – Internet, prohlížení fotek, ne příliš náročné hry pro syna. Počítá se i s digitální kamerou, takže jednoduché zpracování a prohlížení vlastní tvorby.
Sestava – AMD Athlon II X3 440 | MSI 770-G45 | Kingston 2GB DDR3 1333MHz | Sapphire ATI Radeon HD 5670 512MB DDR5 | Seagate Barracuda 7200.12 500GB | Seasonic SS-400ET 400W | Cooler Master Hyper TX3 | LG GH22LS | Cooler Master Elite 335 | Windows 7 Home Premium CZ 64bit OEM | BenQ 23″ G2320HDB | Logitech Deluxe 250 Keyboard Black | Cena: 18 250 Kč

Lukáš Senohrábek	| 24.4.2010
Zadání – Počítač primárně na hry, dál normální využití jako internet, filmy, hudba a zpracování fotografií. Dalšími důležitými faktory byla spolehlivost a tichost sestavy. Předpokládá se využití stávajících periferií a monitoru, ten bude dokoupen později. Doporučen NEC EA231WMi.
Sestava – Intel Core i5-750 | Gigabyte GA-P55A-UD3 | Kingston HyperX 4GB 1333MHz DDR3 | Gigabyte Radeon HD 5850 1GB | Samsung SpinPoint F3 1TB HD103SJ | Corsair HX620 | Cooler Master Hyper 212 Plus | LG GH22LS | Lian Li PC-7FNB | Windows 7 Home Premium CZ 64bit OEM | Edimax EW-7727In | Apacer AE700 Black | Cena: 29 390 Kč

Lukáš Senohrábek	| 12.1.2010
Zadání – kancelářský počítač, nenáročná práce zahrnující internet, kancelářský software, prohlížení fotografií. Požadavek na co nejnižší cenu, to zahrnuje využití stávajících periferií, monitoru a case.
Sestava –  AMD Athlon 64 X2 7750+ Black Edition (využit starší kus za výrazně nižší cenu) | Gigabyte GA-MA74GM-S2H | A-Data 2GB 800MHz | Western Digital Caviar 320GB WD3200AAKS | Seasonic Energy Knight SS-350ET 350W | Arctic-Cooling Alpine 64 Pro |
Cena: 6 230 Kč

Lukáš Senohrábek	| 18.8.2009
Zadání – počítač na internet, okrajově kancelářská práce a hraní starších her, možnost v budoucnu osadit výkonnější grafickou kartou, nízká cena.
Sestava – AMD Athlon II X2 245 | ASRock A780GM-LE | A-Data 2x2GB DDR2 800MHz | Western Digital Caviar 320GB WD3200AAKS | LG GH22NP20 | Seasonic SS-350ET-F3 350W | CoolerMaster Elite 341 | LG 19″ W1942T-PF | Microsoft Basic Optical Mouse Black | Genius SP-J200 2.0 5W | Windows Vista Home Premium 64bit CZ OEM | Cena: 13 730 Kč

Lukáš Senohrábek	| 5.3.2009
Zadání – počítač na internet, hraní nových her a sledování filmů s možností budoucího upgradu. Cena do 20 tisíc Kč s OS, monitorem, klávesnicí a myší, bezezbytku využít cenový potenciál.
Sestava – AMD Athlon 64 X2 7750+ Black Edition | Gigabyte GA-MA770-UD3 | A-Data 2x2GB DDR2 800MHz | Sapphire HD 4830 512MB | Western Digital Caviar 320GB WD3200AAKS | LG GH22LS30 | Seasonic SS-400ET-T3 400W | CoolerMaster Elite 335 | Samsung 20″ 2053BW | Logitech Media Keyboard 600 | Logitech RX1500 | Arctic Cooling Freezer 64 Pro + Arctic Fan 12 PWM | Windows Vista Home Premium 64bit CZ OEM | Cena: 20 100 Kč

Lukáš Senohrábek	| 4.11.2008
Zadání – domácí počítač na kancelářskou práci, internet a občasné hraní, druhotnými faktory bylo ticho a rozšiřitelnost do budoucna. Cena do 20 tisíc Kč s operačním systémem a všemi periferiemi. Po další dohodě se zákazníkem cena PC << 20 000 Kč.
Sestava – AMD Athlon 64 X2 4850+ EE | Gigabyte MA770-DS3 | A-Data 2x1GB DDR2 800MHz | Sapphire HD 4670 512MB | Samsung SpinPoint F1 320GB | LG GH22LS30 | Seasonic SS-350ET-F3 350W | CoolerMaster Elite 334 | Samsung 19″ 953BW | Genius KB-16e | A4tech X-710BK | Genius SP-HF2.0 1100X | Arctic Cooling Freezer 64 Pro + 2x Arctic Fan 12025L | Windows Vista Home Premium 64bit CZ OEM | Cena: 19 000 Kč
