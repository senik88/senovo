<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 21.11.2016
 * Time: 19:45
 */

namespace app\modules\cms;


use app\modules\cms\components\ErrorHandler;
use yii\base\Module;
use yii\base\Theme;

/**
 * Class CmsModule
 * @package app\modules\cms
 */
class CmsModule extends Module
{

    public $layout = '@app/themes/dashboard/layouts/main';

    public function init()
    {
        parent::init();

//        \Yii::$app->view->theme->pathMap = ['@app/views' => '@app/themes/dashboard/views'];
//        \Yii::$app->view->theme->baseUrl = '@web/themes/dashboard';

        \Yii::$app->view->theme = new Theme([
            'pathMap' => ['@app/views' => '@app/themes/dashboard/views'],
            'baseUrl' => '@web/themes/dashboard'
        ]);

        // vlastni error handler
        \Yii::configure($this, [
            'components' => [
                'errorHandler' => [
                    'class' => ErrorHandler::className(),
                    'errorAction' => 'cms/default/error',
                ]
            ],
        ]);

        /** @var ErrorHandler $handler */
        $handler = $this->get('errorHandler');
        \Yii::$app->set('errorHandler', $handler);
        $handler->register();
    }
}