<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 16.12.2018
 * Time: 15:32
 */

namespace app\modules\cms\controllers;


use app\models\Gallery;
use app\modules\cms\components\SenovoController;
use app\modules\cms\forms\GalleryAddForm;
use yii\web\UploadedFile;

/**
 * Class GalleryController
 * @package app\modules\cms\controllers
 */
class GalleryController extends SenovoController
{
    /**
     *
     */
    public function actionIndex()
    {
        $gallery = new Gallery();

        return $this->render('index', [
            'mGallery' => $gallery
        ]);
    }

    public function actionAdd()
    {
        $model = new GalleryAddForm();

        $post = \Yii::$app->request->post();

        if (!empty($post)) {
            $files = UploadedFile::getInstances($model, 'photos_upload');
            if (!empty($files)) {
                $model->setFiles($files);
            }

            if ($model->load($post) && $model->validate()) {

//                \Kint::dump($model->attributes, empty($files), $model->getFiles()); die;

                $model->process();
                return $this->redirect(['/cms/gallery/index']);

            }
        }

        return $this->render('add', [
            'model' => $model
        ]);
    }
}