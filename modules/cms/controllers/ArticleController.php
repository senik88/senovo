<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 24.04.2018
 * Time: 20:12
 */

namespace app\modules\cms\controllers;


use app\components\senovo\Flashes;
use app\models\article\Article;
use app\models\File;
use app\modules\cms\components\SenovoController;
use app\modules\cms\forms\ArticleForm;
use app\modules\cms\models\ArticleFilter;

/**
 * Class ArticleController
 * @package app\modules\cms\controllers
 */
class ArticleController extends SenovoController
{
    /**
     *
     */
    public function actionIndex()
    {
        $filter = new ArticleFilter();

        $model = new Article();
        $model->setFilter($filter);

        return $this->render('index', ['model' => $model, 'filter' => $filter]);
    }

    /**
     *
     */
    public function actionAdd()
    {
        $model = new ArticleForm();

        $post = \Yii::$app->request->post();
        if (!empty($post)) {
            if ($model->load($post) && $model->validate()) {
                $model->process();
                Flashes::addSuccess("Article saved.");

                return $this->redirect(['/cms/article/index']);
            }
        }

        return $this->render('add', ['model' => $model, 'file' => null]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionUpdate($id)
    {
        /** @var Article $article */
        $article = Article::findOne($id);

        $model = new ArticleForm();
        $model->setModel($article);

        $file = null;
        if ($article->picture != null) {
            $file = File::findOne(['hash' => $article->picture]);
        }

        $post = \Yii::$app->request->post();
        if (!empty($post)) {
            if ($model->load($post) && $model->validate()) {
                $model->process();
                Flashes::addSuccess("Article saved.");

                return $this->redirect(['/cms/article/index']);
            }
        }

        return $this->render('update', ['model' => $model, 'file' => $file]);
    }

    /**
     * @param $id
     */
    public function actionDelete($id)
    {

    }

    public function actionChangeOrder()
    {
        $post = \Yii::$app->request->post();

        if (!empty($post)) {
            if (!isset($post['Article'])) {
                Flashes::addError('Invalid data');
                return $this->redirect(['/cms/article/index']);
            }

            // validace?


            $trans = \Yii::$app->db->beginTransaction();
            try {
                foreach ($post['Article'] as $pk => $data) {
                    /** @var Article $mArticle */
                    $mArticle = Article::findOne($pk);

                    if ($mArticle == null) {
                        throw new \Exception("Cannot find Article with pk = $pk");
                    }

                    $mArticle->article_pos = $data['article_pos'];

                    if (!$mArticle->save(false, ['article_pos'])) {
                        throw new \Exception("Cannot save Article with pk = $pk");
                    }
                }

                $trans->commit();

                Flashes::addSuccess("Successfuly changed articles order");
                return $this->redirect(['/cms/article/index']);
            } catch (\Exception $e) {
                Flashes::addError('Error: ' . $e->getMessage());
                $trans->rollBack();
            }
        }

        Flashes::addInfo("Nothing happened");
        return $this->redirect(['/cms/article/index']);
    }
}