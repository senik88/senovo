<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 21.11.2016
 * Time: 19:48
 */

namespace app\modules\cms\controllers;


use app\components\senovo\Flashes;
use app\models\LoginForm;
use app\models\reference\Reference;
use app\models\reference\type\TypeFactory;
use app\modules\cms\components\SenovoController;
use app\modules\cms\forms\NewReferenceForm;
use app\modules\cms\models\ReferenceFilter;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Class DefaultController
 * @package app\modules\admin\controllers
 */
class ReferenceController extends SenovoController
{
    /**
     *
     */
    public function actionIndex()
    {
        $filter = new ReferenceFilter();
        $getData = Yii::$app->request->get();

        if (!empty($getData)) {
            $filter->load($getData);
        }

        $model = new Reference();
        $model->setFilter($filter);

        return $this->render('index', [
            'filter' => $filter,
            'model' => $model
        ]);
    }

    /**
     *
     */
    public function actionAdd()
    {
        $model = new NewReferenceForm();

        $post = Yii::$app->request->post();
        if (!empty($post)) {
            if ($model->load($post) && $model->validate()) {
                $model->process();
                Flashes::addSuccess("Reference uložena.");

                return $this->redirect(['/cms/reference/index']);
            }
        }

        return $this->render('add', [
            'model' => $model
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionUpdate($id)
    {
        /** @var Reference $reference */
        $reference = Reference::findOne($id);

        $model = new NewReferenceForm();
        $model->setReference($reference);

        $post = Yii::$app->request->post();
        if (!empty($post)) {
            if ($model->load($post) && $model->validate()) {
                $model->process();
                Flashes::addSuccess("Reference updatována.");

                return $this->redirect(['/cms/reference/index']);
            }
        }

        return $this->render('update', [
            'model' => $model
        ]);
    }

    /**
     * @param $id
     */
    public function actionDelete($id)
    {

    }
}