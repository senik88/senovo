<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 28.04.2018
 * Time: 17:44
 */

namespace app\modules\cms\controllers;


use app\components\FileManager;
use app\models\File;
use app\modules\cms\components\SenovoController;

/**
 * Class FilesController
 * @package app\modules\cms\controllers
 */
class FilesController extends SenovoController
{
    /**
     * @param string $p
     * @return string
     */
    public function actionIndex($p = null)
    {
        $manager = new FileManager();

        $manager->setPath($p);

//        \Kint::dump($manager->getFiles()); die;

        return $this->render('index', [
            'manager' => $manager
        ]);
    }

    public function actionTest()
    {
        $file = File::findOne(13);
    }
}