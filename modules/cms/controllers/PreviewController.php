<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 13.12.2018
 * Time: 17:57
 */

namespace app\modules\cms\controllers;


use app\models\reference\Reference;
use app\modules\cms\components\SenovoController;

class PreviewController extends SenovoController
{

    public $layout = '@app/views/layouts/preview';

    /**
     * @param $id
     * @return string
     */
    public function actionReference($id)
    {
        $mReference = Reference::findOne($id);

        return $this->render('reference', [
            'mReference' => $mReference
        ]);
    }
}