<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 15.01.2017
 * Time: 18:01
 */

namespace app\modules\cms\components;


use yii\filters\AccessControl;
use yii\web\Controller;

class SenovoController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
//                    [
//                        'allow' => true,
//                        'roles' => ['?'],
//                        'actions' => ['login']
//                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
}