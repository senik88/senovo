<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 16.12.2018
 * Time: 18:26
 */

namespace app\modules\cms\forms;


use app\components\senovo\Form;
use app\models\File;
use app\models\Gallery;
use app\models\photo\Photo;
use app\models\photo\PhotoFileHandler;
use app\models\photo\SizeFactory;
use Imagine\Imagick\Image;
use Imagine\Imagick\Imagine;
use yii\web\UploadedFile;

/**
 * Class GalleryAddForm
 * @package app\modules\cms\forms
 *
 */
class GalleryAddForm extends Form
{

    public $name;

    public $description;

    public $publish;

    public $photogallery_id;

    public $photos_upload;

    public $photos;

    public $sizes; // asi by bylo vhodne ulozit i do galerie, klidne jako JSON

    public $path = '/';

    /** @var  UploadedFile[] */
    protected $_files;

    /** @var PhotoFileHandler */
    protected $photoHandler;

    /**
     *
     */
    public function init()
    {
        parent::init();
        $this->photoHandler = new PhotoFileHandler();
    }

    /**
     *
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description', 'publish', 'photogallery_id', 'photos_upload', 'sizes', 'path'], 'safe']
        ];
    }

    /**
     * @param \yii\web\UploadedFile[] $files
     */
    public function setFiles($files)
    {
        $this->_files = $files;
    }

    public function getFiles() {
        return $this->_files;
    }

    /**
     * todo az se jednou rozhodnu to cely refactorovat, tak do service... ale to az mi to nekdo zaplati :D
     * @throws \Exception
     */
    public function process()
    {
        $trans = \Yii::$app->db->beginTransaction();

        try {
            $gallery = new Gallery();
            $gallery->name = $this->name;
            $gallery->description = $this->description;
            $gallery->photogallery_id = $this->photogallery_id;
            $gallery->published = $this->publish;

            if (!$gallery->save()) {
                \Kint::dump($gallery->getErrors()); die;
            }

            $gallery->refresh();
            // zprocesuju jednotlive soubory
            if (is_array($this->_files) && !empty($this->_files)) {
                foreach ($this->_files as $file) {
                    $photo = $this->processSingleFileWithSizes($file, $this->sizes);
                    $photo->photogallery_pk = $gallery->photogallery_pk;
                    $photo->save();
                }
            }

            $trans->commit();
        } catch (\Exception $e) {
            $trans->rollBack();
            throw $e;
        }
    }

    /**
     * @param UploadedFile $upFile
     * @param $sizes
     * @return Photo
     */
    protected function processSingleFileWithSizes($upFile, $sizes)
    {
        $photo = new Photo();
        $image = (new Imagine())->open($upFile->tempName);

        foreach ($sizes as $size) {
            if (!$this->processSingleSize($size, $photo, $upFile, $image)) {
                throw new \RuntimeException(sprintf('nepovedlo se zpracovat obrazek velikosti %s', $size));
            }
        }

        return $photo;
    }

    /**
     * @param string $sizeAbbr
     * @param Photo $photo
     * @param UploadedFile $upFile
     * @param $image
     * @return bool
     */
    protected function processSingleSize($sizeAbbr, $photo, $upFile, $image)
    {
        $size = SizeFactory::fromAbbrv($sizeAbbr);
        $size->setImage($image);

        if ($sizeAbbr === SizeFactory::SIZE_ABBR_ORIGINAL) {
            $photo->original_width = $size->getWidth();
            $photo->original_height = $size->getHeight();
        }

        $size->processImage();

        $file = new File();
        $file->name = $upFile->name;
        $file->mimetype = $upFile->type;
        $file->uploaded = date('Y-m-d H:i:s e', time());
        $file->path = $this->path;

        $this->photoHandler->setFile($file);
        $this->photoHandler->setImage($size->getImage());
        $this->photoHandler->handle();

        if (!$file->save()) {
            throw new \RuntimeException(sprintf('nepovedlo se ulozit soubor do DB'));
        }

        $fkName = sprintf('file_%s_pk', lcfirst(Photo::getSizes()[$sizeAbbr]));

        $photo->$fkName = $file->file_pk;
        return true;
    }

    /**
     * @return mixed
     */
    public function loadValues()
    {
        // TODO: Implement loadValues() method.

        // podle id vytáhnout data z DB

        // naplnit galerii obrázky
    }
}