<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 02.04.2018
 * Time: 10:30
 */

namespace app\modules\cms\forms;


use app\components\senovo\Form;
use app\models\reference\Reference;

/**
 * Class ReferenceForm
 * @package app\modules\cms\forms
 */
class NewReferenceForm extends Form
{
    public $type;

    public $date;

    public $content;

    public $link;

    public $publish;

    public $gallery_pk;

    /**
     * @var Reference
     */
    protected $_reference;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'date', 'content', 'link', 'publish', 'gallery_pk'], 'safe']
        ];
    }

    public function init()
    {
        $id = \Yii::$app->request->get('id');
        if ($id != null) {
            $model = Reference::findOne(['reference_pk' => $id]);
        } else {
            $model = new Reference();
        }

        $this->setReference($model);
        $this->loadValues();
    }

    /**
     *
     */
    public function process()
    {
        $this->_reference->type = $this->type;
        $this->_reference->ref_date = $this->date;
        $this->_reference->content = $this->content;
        $this->_reference->link = $this->link;
        $this->_reference->published = $this->publish;
        $this->_reference->photogallery_pk = $this->gallery_pk;

        if (!$this->_reference->save()) {
            throw new \Exception('Cannot save reference');
        }
    }

    /**
     * @param Reference $reference
     */
    public function setReference($reference)
    {
        $this->_reference = $reference;
    }

    public function loadValues()
    {
        $this->type = $this->_reference->type;
        $this->date = $this->_reference->ref_date !== null ? date('Y-m-d', strtotime($this->_reference->ref_date)) : null;
        $this->content = $this->_reference->content;
        $this->link = $this->_reference->link;
        $this->publish = $this->_reference->published != null ? date('Y-m-d H:i', strtotime($this->_reference->published)) : null;
        $this->gallery_pk = $this->_reference->photogallery_pk;
    }
}