<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 25.04.2018
 * Time: 20:27
 */

namespace app\modules\cms\forms;


use app\components\senovo\Form;
use app\components\senovo\helpers\DbHelper;
use app\models\article\Article;
use app\models\File;
use yii\web\UploadedFile;

/**
 * Class ArticleForm
 * @package app\modules\cms\forms
 */
class ArticleForm extends Form
{
    /**
     * @var
     */
    public $article_id;

    /**
     * @var
     */
    public $title;

    /**
     * @var
     */
    public $content;

    /**
     * @var
     */
    public $picture;

    /**
     * @var
     */
    public $picture_pos;

    /**
     * @var
     */
    public $show_up;

    /**
     * @var
     */
    public $article_pos;

    /**
     * @var
     */
    public $publish;

    /**
     * @var Article
     */
    protected $_article;

    /**
     *
     */
    public function init()
    {
        $id = \Yii::$app->request->get('id');
        if ($id != null) {
            $model = Article::findOne(['article_pk' => $id]);
        } else {
            $model = new Article();
        }

        $this->setModel($model);
        $this->loadValues();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [$this->attributes(), 'safe']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'article_id'  => 'Article ID',
            'article_pos' => 'Article position',
            'picture_pos' => 'Picture position',
            'show_up'     => 'Show "up" button'
        ];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function process()
    {
        $oldPos = $this->_article->article_pos;
        $newPos = (int) $this->article_pos;

        $this->_article->article_id     = $this->article_id;
        $this->_article->article_pos    = $this->article_pos;
        $this->_article->content        = $this->content;
        $this->_article->title          = $this->title;
        $this->_article->picture_pos    = $this->picture_pos;
        $this->_article->show_up        = $this->show_up;
        $this->_article->published      = $this->publish;

        // musim zpracovat obrazek
//        $this->picture = $this->_article->picture;

//        \Kint::dump($oldPos, $newPos); die;

        $trans = \Yii::$app->db->beginTransaction();

        try {
            if (($picture = UploadedFile::getInstance($this, 'picture')) != null) {
                $mFile = new File();
                $mFile->file_pk = DbHelper::newPkey('file', 'file_pk');
                $mFile->name = $picture->name;

                // check path
                $path = $mFile->getAbsolutePath();
                if (!file_exists($path)) { mkdir($path); }
                if (!is_writable($path)) { chmod($path, 0777); }

                $path .= DIRECTORY_SEPARATOR . Article::getPathInRuntime();
                if (!file_exists($path)) { mkdir($path); }
                if (!is_writable($path)) { chmod($path, 0777); }

                $mFile->path = Article::getPathInRuntime(); // budu zapisovat relativni cestu
                $mFile->size = $picture->size;
                $mFile->mimetype = $picture->type;
                $mFile->hash = md5($mFile->name.'|'.$mFile->size.'|'.$mFile->mimetype);
                $mFile->uploaded = date('Y-m-d H:i:s');

                $saved = $picture->saveAs($path . DIRECTORY_SEPARATOR . $mFile->file_pk);
                if (!$saved) {
                    throw new \Exception('Can not move uploaded file');
                }

                $saved &= $mFile->save();
                if (!$saved) {
                    throw new \Exception('File could not be saved');
                }

                // zmena poradi // at uz novy nebo stary
                if ($oldPos === 0 || $newPos !== $oldPos) {

                }

                $this->_article->picture = $mFile->hash;
            }

            if (!$this->_article->save()) {
                throw new \Exception('Cannot save article');
            }

            $trans->commit();
        } catch (\Exception $e) {
            \Kint::dump($e->getMessage()); die;
            $trans->rollBack();
        }
    }

    /**
     * @param Article $article
     */
    public function setModel($article)
    {
        $this->_article = $article;
    }

    /**
     *
     */
    public function loadValues()
    {
        $this->article_id = $this->_article->article_id;
        $this->article_pos = $this->_article->article_pos;
        $this->content = $this->_article->content;
        $this->title = $this->_article->title;
        $this->picture = $this->_article->picture;
        $this->picture_pos = $this->_article->picture_pos;
        $this->show_up = $this->_article->show_up;
        $this->publish = date('Y-m-d H:i', strtotime($this->_article->published));
    }
}