<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 01.04.2018
 * Time: 22:28
 *
 * @var $model \app\modules\cms\forms\ArticleForm
 */
?>

<div class="row">
    <div class="col-md-12">
        <div class="h1-buttons clearfix">
            <h1>Add new article</h1>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $this->render('_form', ['model' => $model, 'file' => null]) ?>
    </div>
</div>