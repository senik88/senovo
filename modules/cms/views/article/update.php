<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 01.04.2018
 * Time: 22:28
 *
 * @var $model ArticleForm
 * @var $file File
 */

use app\models\File;
use app\modules\cms\forms\ArticleForm;
?>

<div class="row">
    <div class="col-md-12">
        <div class="h1-buttons clearfix">
            <h1>Update article <?= $model->title ?></h1>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $this->render('_form', ['model' => $model, 'file' => $file]) ?>
    </div>
</div>