<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 25.04.2018
 * Time: 20:44
 *
 * @var $model ArticleForm
 * @var $file File
 */

use app\models\File;
use app\modules\cms\forms\ArticleForm;
use kartik\switchinput\SwitchInput;
use kartik\widgets\DateTimePicker;
use kartik\widgets\FileInput;
use vova07\imperavi\Widget as Redactor;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
?>

<?php
$form = ActiveForm::begin([
    'id' => 'article-add-form',
    'layout' => 'default',
    'method' => 'POST',
    'options' => [
        'enctype' => 'multipart/form-data'
    ]
]);
?>
<div class="form-fields">
    <div class="row">
        <div class="col-md-3"><?= $form->field($model, 'title') ?></div>
        <div class="col-md-3"><?= $form->field($model, 'article_id') ?></div>
        <div class="col-md-3"><?= $form->field($model, 'article_pos')->textInput(['type' => 'number', 'min' => "1", 'step' => "1"]) ?></div>
        <div class="col-md-3"><?= $form->field($model, 'show_up')->widget(SwitchInput::className(), ['type' => SwitchInput::CHECKBOX]) ?></div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'content')->widget(Redactor::className(), [
                'settings' => [
                    'lang' => 'cs',
                    'minHeight' => 200,
                    'plugins' => [
                        'fullscreen', 'table'
                    ]
                ]
            ]); ?>
        </div>
    </div>

    <!--  todo filewidget  -->
    <?php if ($file instanceof File) { ?>
    <div class="row">
        <div class="col-md-12">
            <?= $file->name ?>
        </div>
    </div>
    <?php } ?>

    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'picture')->widget(FileInput::className(), [
            'pluginOptions' => [
                'showPreview' => false,
                'showCaption' => true,
                'showRemove' => true,
                'showUpload' => false
            ]
        ]) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'picture_pos')->widget(SwitchInput::className(), [
            'type' => SwitchInput::RADIO,
            'items' => [
                ['label' => 'Left', 'value' => 'LEFT'],
                ['label' => 'Center', 'value' => 'CENTER'],
                ['label' => 'Right', 'value' => 'RIGHT'],
            ],
            'pluginOptions' => ['size' => 'mini'],
        ]) ?></div>
    </div>

    <div class="row">
        <div class="col-md-3"><?= $form->field($model, 'publish')->widget(DateTimePicker::className(), [
            'convertFormat' => true,
            'pluginOptions' => [
                'startView' => 1,
                'minView' => 0,
                'maxView' => 1,
                'autoclose' => true,
                'format' => 'php:Y-m-d H:i'
            ]
        ]) ?></div>
    </div>
</div>

<div class="form-actions">
    <div class="row">
        <div class="col-md-12">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>
    </div>
</div>
<?php
$form->end();
?>