<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 21.11.2016
 * Time: 19:59
 *
 * @var $filter ReferenceFilter
 * @var $model Article
 */

use app\models\article\Article;
use app\modules\cms\models\ReferenceFilter;
use dosamigos\datepicker\DateRangePicker;
use yii\bootstrap\ActiveForm;
?>

<div class="row">
    <div class="col-md-12">
        <div class="h1-buttons clearfix">
            <h1>Articles</h1>
            <?= \yii\bootstrap\Html::a('Add', ['/cms/article/add'], ['class' => 'btn btn-primary btn-sm']) ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?php
        $form = ActiveForm::begin([
            'id' => 'article-filter',
            'layout' => 'default',
            'method' => 'GET',
            'action' => \yii\helpers\Url::to(['/cms/article/index']),
            'options' => ['class' => 'well']
        ]);
        ?>
        <div class="form-fields">
            <div class="row">
                <div class="col-md-3"><?= $form->field($filter, 'id') ?></div>
                <div class="col-md-3"><?= $form->field($filter, 'title') ?></div>
                <div class="col-md-3"><?= $form->field($filter, 'content') ?></div>
                <div class="col-md-3"><?= $form->field($filter, 'published_from')->widget(DateRangePicker::className(), [
                        'attributeTo' => 'published_to',
                        'form' => $form, // best for correct client validation
                        'clientOptions' => [
                            'startView' => 1,
                            'minView' => 0,
                            'maxView' => 1,
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd'
                        ]
                    ])->label($model->getAttributeLabel('published')) ?></div>
            </div>
        </div>

        <div class="form-actions">
            <div class="row">
                <div class="col-md-12">
                    <?= \yii\bootstrap\Html::submitButton('Filter', ['class' => 'btn btn-success btn-sm']) ?>
                </div>
            </div>
        </div>
        <?php
        $form->end();
        ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?php
        $form = ActiveForm::begin([
            'id' => 'article-postions-form',
            'layout' => 'default',
            'method' => 'POST',
            'action' => \yii\helpers\Url::to(['/cms/article/change-order']),
        ]);
        ?>

        <?=
        \yii\grid\GridView::widget([
            'dataProvider' => $model->getDataProvider(),
            'columns' => $model->getColumns()
        ]);
        ?>
        <div class="form-actions pull-right">
            <?= \yii\bootstrap\Html::submitButton('Change order', ['class' => 'btn btn-success btn-sm']) ?>
        </div>
        <?php
        $form->end();
        ?>
    </div>
</div>