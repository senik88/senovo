<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 13.12.2018
 * Time: 18:05
 *
 * @var $this View
 * @var $mReference Reference
 */
use app\models\reference\Reference;
use yii\web\View;

?>

<div class="site-index">
    <div class="body-reference" id="reference">
        <div class="container">
            <div id="reference-wrapper">
                <?php
                echo $this->render('@app/views/site/_reference', ['mReference' => $mReference]);
                ?>
            </div>
        </div>
    </div>
</div>