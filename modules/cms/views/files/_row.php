<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 28.04.2018
 * Time: 18:08
 *
 * @var $model File
 * @var $manager FileManager
 */

use app\components\FileManager;
use app\models\File;
use yii\bootstrap\Html;

$filename = $model->name;
$actPath = Yii::$app->request->get('p'); // todo predelat tuhle hruzu
if ($model->mimetype == 'directory') {
    $path = $manager->getRelativePath();
    if ($path == '') {
        $path = $model->name;
        $filename = Html::a($model->name, ['/cms/files/index', 'p' => $path]);
    } else if ($path = '..') {
        $path = substr($path, 0, strrpos($path, '/'));
        if ($path == null) {
            $filename = Html::a($model->name, ['/cms/files/index']);
        } else {
            $filename = Html::a($model->name, ['/cms/files/index', 'p' => $path]);
        }
    } else {
        $path = $manager->getRelativePath() . DIRECTORY_SEPARATOR . $model->name;
        $filename = Html::a($model->name, ['/cms/files/index', 'p' => $path]);
    }
}

$showActions = ($model->mimetype !== 'directory');
?>

<tr>
    <td>
        <label for="file-<?= $model->name ?>"><input type="checkbox" name="checkedFiles" id="file-<?= $model->name ?>" value="<?= $model->name ?>"></label>
    </td>
    <td><?= $filename ?></td>
    <td><?= $model->size ?></td>
    <td></td>
    <td></td>
    <td></td>
    <td>
        <?php if ($showActions): ?>
        <?= Html::a(Html::icon('download'), ['/site/file', 'hash' => $model->hash], ['title' => 'Stáhnout']) ?>
        <?php endif; ?>
    </td>
</tr>