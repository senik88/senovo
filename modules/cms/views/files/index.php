<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 28.04.2018
 * Time: 18:10
 *
 * @var $manager FileManager
 *
 */

use app\components\FileManager;
?>

<table id="file-browser">
    <tr>
        <th></th>
        <th>Name</th>
        <th>Size</th>
        <th>Modified</th>
        <th>Perms</th>
        <th>Owner</th>
        <th>Actios</th>
    </tr>
    <?php
    echo \yii\widgets\ListView::widget([
        'dataProvider' => $manager->getFiles(),
        'itemView' => '_row',
        'viewParams' => ['manager' => $manager],
    ])
    ?>
</table>