<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 16.12.2018
 * Time: 15:33
 *
 * @var $mGallery Gallery
 */

use app\models\Gallery;
use yii\widgets\ListView;

?>


<div class="row">
    <div class="col-md-12">
        <div class="h1-buttons clearfix">
            <h1>Photogalleries</h1>
            <?= \yii\bootstrap\Html::a('Add', ['/cms/gallery/add'], ['class' => 'btn btn-primary btn-sm']) ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?php
        echo ListView::widget([
            'dataProvider' => $mGallery->getDataProvider(),
            'itemView' => '_list'
        ]);
        ?>
    </div>
</div>