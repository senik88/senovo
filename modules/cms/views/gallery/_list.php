<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 25.12.2018
 * Time: 21:48
 *
 * @var $this View
 * @var $model Gallery
 */

use app\assets\FancyBoxAsset;
use app\models\File;
use app\models\Gallery;
use app\models\photo\Photo;
use yii\bootstrap\Html;
use yii\web\View;

FancyBoxAsset::register($this);
?>

<div class="row gallery-container">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <h3><?= $model->name ?></h3>
                <?= $model->description ?>
                <p>
                    Obsahuje tyto velikosti: <?= implode(', ', $model->sizes) ?>
                </p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
            <?php
            if ($model->hasThumbnails()) {
                /** @var Photo $photo */
                foreach ($model->photos as $photo) {
                    /** @var File $thumb */
                    $thumb = $photo->thumbnail;
                    /** @var File $orig */
                    $orig = $photo->original;

                    echo Html::a(
                        Html::img(['/site/file', 'hash' => $thumb->hash], ['class' => 'img-thumbnail']),
                        ['/site/file', 'hash' => $orig->hash],
                        ['data-fancybox' => $model->photogallery_id, 'rel' => $model->photogallery_id]
                    );
                }
            }
            ?>
            </div>
        </div>
    </div>
</div>