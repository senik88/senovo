<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 16.12.2018
 * Time: 18:46
 */

use dosamigos\datepicker\DatePicker;
use kartik\widgets\DateTimePicker;
use vova07\imperavi\Widget as Redactor;
use yii\bootstrap\ActiveForm;

$form = ActiveForm::begin([
    'id' => 'gallery-add-form',
    'layout' => 'default',
    'method' => 'POST',
    'options' => [
        'enctype' => 'multipart/form-data'
    ]
]);
?>
    <div class="form-fields">
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'description')->widget(Redactor::className(), [
                    'settings' => [
                        'lang' => 'cs',
                        'minHeight' => 200,
                        'plugins' => [
                            'fullscreen', 'table'
                        ]
                    ]
                ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'name') ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'photogallery_id') ?>
            </div>
            <div class="col-md-3">
                <!-- todo udelat picker z uploaded files -->
                <?= $form->field($model, 'path')->label('Filepath') ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'publish')->widget(DateTimePicker::className(), [
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'startView' => 1,
                        'minView' => 0,
                        'maxView' => 1,
                        'autoclose' => true,
                        'format' => 'php:Y-m-d H:i'
                    ]
                ]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <h3>Add photos</h3>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <?= $form->field($model, 'photos_upload[]')->widget(\kartik\widgets\FileInput::class, [
                    'options' => [
                        'multiple' => true,
                        'accept' => 'image/*'
                    ]
                ]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'sizes')->checkboxList(\app\models\photo\Photo::getSizes()) ?>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <?= \yii\bootstrap\Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div>
<?php $form->end(); ?>