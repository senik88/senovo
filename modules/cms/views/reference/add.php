<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 01.04.2018
 * Time: 22:28
 *
 * @var $model \app\modules\cms\forms\NewReferenceForm
 * @var $this View
 */

use yii\web\View;
?>

<div class="row">
    <div class="col-md-12">
        <div class="h1-buttons clearfix">
            <h1>Add new reference</h1>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $this->render('_form', ['model' => $model]) ?>
    </div>
</div>