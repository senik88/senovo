<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 21.11.2016
 * Time: 19:59
 *
 * @var $filter ReferenceFilter
 * @var $model Reference
 * @var $this View
 */

use app\models\reference\Reference;
use app\models\reference\type\TypeFactory;
use app\modules\cms\models\ReferenceFilter;
use dosamigos\datepicker\DateRangePicker;
use yii\bootstrap\ActiveForm;
use yii\web\View;

?>

<div class="row">
    <div class="col-md-12">
        <div class="h1-buttons clearfix">
            <h1>References</h1>
            <?= \yii\bootstrap\Html::a('Add', ['/cms/reference/add'], ['class' => 'btn btn-primary btn-sm']) ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?php
        $form = ActiveForm::begin([
            'id' => 'reference-filter',
            'layout' => 'default',
            'method' => 'GET',
            'action' => \yii\helpers\Url::to(['/cms/reference/index']),
            'options' => ['class' => 'well']
        ]);
        ?>
        <div class="form-fields">
            <div class="row">
                <div class="col-md-3"><?= $form->field($filter, 'type')->dropDownList(['' => 'All'] + TypeFactory::getMapping()) ?></div>
                <div class="col-md-3"><?= $form->field($filter, 'date_from')->widget(DateRangePicker::className(), [
                        'attributeTo' => 'date_to',
                        'form' => $form, // best for correct client validation
                        'clientOptions' => [
                            'startView' => 1,
                            'minView' => 0,
                            'maxView' => 1,
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd'
                        ]
                    ])->label($model->getAttributeLabel('date')) ?></div>
                <div class="col-md-3"><?= $form->field($filter, 'content') ?></div>
                <div class="col-md-3"><?= $form->field($filter, 'published_from')->widget(DateRangePicker::className(), [
                        'attributeTo' => 'published_to',
                        'form' => $form, // best for correct client validation
                        'clientOptions' => [
                            'startView' => 1,
                            'minView' => 0,
                            'maxView' => 1,
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd'
                        ]
                    ])->label($model->getAttributeLabel('published')) ?></div>
            </div>
        </div>

        <div class="form-actions">
            <div class="row">
                <div class="col-md-12">
                    <?= \yii\bootstrap\Html::submitButton('Filter', ['class' => 'btn btn-success btn-sm']) ?>
                </div>
            </div>
        </div>
        <?php
        $form->end();
        ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?=
        \yii\grid\GridView::widget([
            'dataProvider' => $model->getDataProvider(),
            'columns' => $model->getColumns()
        ]);
        ?>
    </div>
</div>

<?php
$this->registerJs(<<<JS
function resizeIframe(obj) {
    obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
}

$('.link-reference-preview').on('click', function(e) {
    e.preventDefault();

    var target = $(this).attr('data-target'),
        previewUrl = $(this).attr('href');//
    // scrolling="no" onload="resizeIframe(this)"
    $(target).find('.modal-body').append('<iframe width="100%" height="300" src="'+previewUrl+'" frameborder="0" allowfullscreen=""></iframe>');
    $(target).modal('show');
});


$('#reference-modal').on('hidden.bs.modal', function (e) {
    $(this).find('.modal-body').html('')
});
JS
);


$modal = \yii\bootstrap\Modal::begin([
    'id' => 'reference-modal',
    'header' => 'Reference preview'
]);

\yii\bootstrap\Modal::end();