<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 24.04.2018
 * Time: 21:33
 */

use app\models\reference\type\TypeFactory;
use dosamigos\datepicker\DatePicker;
use kartik\widgets\DateTimePicker;
use vova07\imperavi\Widget as Redactor;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$form = ActiveForm::begin([
    'id' => 'reference-add-form',
    'layout' => 'default',
    'method' => 'POST',
//    'action' => Url::to(['/cms/reference/' . ]),
]);
?>
<div class="form-fields">
    <div class="row">

    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'content')->widget(Redactor::className(), [
                'settings' => [
                    'lang' => 'cs',
                    'minHeight' => 200,
                    'plugins' => [
                        'fullscreen', 'table'
                    ]
                ]
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3"><?= $form->field($model, 'type')->dropDownList(TypeFactory::getMapping()) ?></div>
        <div class="col-md-3"><?= $form->field($model, 'date')->widget(DatePicker::className(), [
                'clientOptions' => [
                    'startView' => 1,
                    'minView' => 0,
                    'maxView' => 1,
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]) ?></div>
        <div class="col-md-3"><?= $form->field($model, 'link') ?></div>
        <div class="col-md-3"><?= $form->field($model, 'publish')->widget(DateTimePicker::className(), [
                'convertFormat' => true,
                'pluginOptions' => [
                    'startView' => 1,
                    'minView' => 0,
                    'maxView' => 1,
                    'autoclose' => true,
                    'format' => 'php:Y-m-d H:i'
                ]
            ]) ?></div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'gallery_pk')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Gallery::find()->all(), 'photogallery_pk', 'name'), [
                'prompt' => 'Žádná galerie'
            ]) ?>
        </div>
    </div>
</div>

<div class="form-actions">
    <div class="row">
        <div class="col-md-12">
            <?= \yii\bootstrap\Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>
    </div>
</div>
<?php $form->end(); ?>