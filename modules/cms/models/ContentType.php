<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 30.12.2016
 * Time: 21:26
 */

namespace app\modules\cms\models;


use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;

/**
 * Class ContenType
 * @package app\modules\cms\models
 *
 * @property integer $content_type_pk
 * @property string  $content_type_id
 * @property string  $title
 * @property integer $layout_pk
 */
class ContentType extends ActiveRecord
{
    /**
     * @var
     */
    public $fields;

    /**
     * @return array
     */
    public static function primaryKey()
    {
        return ['content_type_pk'];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'content_type';
    }

    /**
     * @return ActiveDataProvider
     */
    public function search()
    {
        return new ActiveDataProvider([
            'query' => $this->find()
        ]);
    }
}