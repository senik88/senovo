<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 24.04.2018
 * Time: 20:18
 */

namespace app\modules\cms\models;


use app\components\senovo\Filter;

/**
 * Class ArticleFilter
 * @package app\modules\cms\models
 */
class ArticleFilter extends Filter
{
    public $id;

    public $title;

    public $content;

    /**
     * @var
     */
    public $published_from;

    /**
     * @var
     */
    public $published_to;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['id', 'title', 'content', 'published_from', 'published_to'], 'safe'],
            [['published_from', 'published_to'], 'date']
        ];
    }
}