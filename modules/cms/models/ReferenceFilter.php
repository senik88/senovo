<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 01.04.2018
 * Time: 20:56
 */

namespace app\modules\cms\models;

use app\components\senovo\Filter;

/**
 * Class ReferenceFilter
 * @package app\modules\cms\models
 */
class ReferenceFilter extends Filter
{
    /**
     * @var
     */
    public $type;

    /**
     * @var
     */
    public $date_from;

    /**
     * @var
     */
    public $date_to;

    /**
     * @var
     */
    public $content;

    /**
     * @var
     */
    public $published_from;

    /**
     * @var
     */
    public $published_to;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['type', 'date_from', 'date_to', 'content', 'published_from', 'published_to'], 'safe'],
            [['date_from', 'date_to', 'published_from', 'published_to'], 'date']
        ];
    }
}