<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 21.11.2016
 * Time: 20:02
 */

namespace app\modules\api\controllers;

use yii\web\Controller;

/**
 * Class DefaultController
 * @package app\modules\api\controllers
 */
class DefaultController extends Controller
{
    /**
     * @param $ver integer
     */
    public function actionIndex($ver)
    {
        \Kint::dump('api/index', $ver);
    }
}