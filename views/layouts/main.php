<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\bootstrap\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

// todo nacitat nekde z konfigurace - kontaktni udaje
$ulice = 'Všestary 235';
$mesto = 'Všestary';
$psc = '503 12';

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Language" content="cs">
    <meta name="Language" content="cs">
    <meta name="Keywords" content="Servis PC, Servis Notebooků, Domácí opravář, PC, počítač, servis počítačů, servis,
                                   prodej, prodej počítačů, prodej notebooků, sleva, Všestary, Hradec Králové, kraj, Královehradecký,
                                   prodej a servis PC, prodej a servis notebooků, počítačové, sítě, návrh, realizace,
                                   počítačové sítě, konzltace, konzultační, činnost, poradenská, poradenská činnost,
                                   opravář, domácí, herní počítač, herní pc, stavba počítačů, stavba pc, počítač herní,
                                   sestavení pc, sestavení počítače, čištění pc, čištění notebooků, čištění">
    <meta name="Description" content="Senovo.cz se zabývá především prodejem a servisem PC a notebooků, tvorbou webů a informačních systémů, provádí návrh a realizuje počítačové sítě. To vše včetně poradenské a konzultační činnost.">
    <meta name="Copyright" content="© Ing. Lukáš Senohrábek <?= date('Y') ?>">
    <meta name="Robots" content="index,follow">
    <meta name="Author" content="www.senovo.cz">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/favicon.ico" type="image/x-icon" />
<!--    <script src='https://www.google.com/recaptcha/api.js'></script>-->
    <script type="text/javascript">
        let basePath = '<?= strtr(Yii::$app->request->getAbsoluteUrl(), ['http:/' => 'https:/']) ?>';
    </script>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    $items = \app\models\Homepage::getMenu();
    NavBar::begin([
        'brandLabel' => 'Senovo.cz',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar', // navbar-fixed-top
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $items,
    ]);
    NavBar::end();
    ?>

    <!-- Vlastni obsah stranky -->
    <?= $content ?>
</div>

<footer class="footer">
    <div class="container">
        <div class="col-md-3 col-sm-6" id="footer-menu">
            <?php
            if (!empty($items)) {
                echo Html::tag('h3', 'Menu');
                echo Html::beginTag('ul');

                foreach ($items as $item) {
                    echo Html::tag('li', Html::a($item['label'], $item['url']), $item['options']);
                }

                echo Html::endTag('ul');
            }
            ?>
        </div>

        <div class="col-md-3 col-sm-6" id="footer-contact">
            <h3 id="kontakt">Kontakt</h3>
            <ul>
                <li><?= Html::icon('user') ?>Ing. Lukáš Senohrábek</li>
                <li><?= Html::icon('home') ?><?= $ulice ?></li>
                <li><?= Html::icon('home') ?><?= $psc ?> <?= $mesto ?></li>
                <li><?= Html::icon('phone-alt') ?>+420 728 190 280</li>
                <li><?= Html::icon('envelope') ?><a href="mailto:lukas.senohrabek@email.cz">lukas.senohrabek(at)email.cz</a></li>
                <li><?= Html::icon('user') ?>IČ 08737975, podnikatel zapsán v živnostenském rejstříku Hradec Králové</li>
            </ul>
        </div>

        <div class="col-md-6 col-sm-12">
            <h3>Zanechte zprávu</h3>
            <p>
                Máte zájem o mé služby, případně potřebujete konzultaci ohledně mých služeb? Neváhejte mne kontaktovat pomocí <a href="mailto:lukas.senohrabek@email.cz?subject=Zpráva ze Senovo.cz">emailu</a>.
            </p>
            <?php
            /** @var \app\models\ContactForm $model */
            $model = $this->params['contactForm'];

            Modal::begin([
                'id' => 'contact-modal',
                'header' => 'Napište nám'
            ]);

            echo $this->render('@app/views/site/_contact', ['model' => $model]);

            Modal::end();
            ?>
        </div>
    </div>

    <!-- Zobrazení mapy provozovny nebo firmy -->
    <script src="//api.mapy.cz/loader.js"></script>
    <script>Loader.load();</script>
    <div id="mapa"></div>
    <script>
        var xcoord,
            ycoord;

        var createMap = function() {
            var center = SMap.Coords.fromWGS84(xcoord, ycoord);
            var mapa = new SMap(JAK.gel("mapa"), center, 16);
            mapa.addDefaultLayer(SMap.DEF_BASE).enable();
            var l = [];
            for(var i = 1; i < l.length; i++) {
                mapa.addDefaultLayer(l[i]).enable();
            }
            var layer = new SMap.Layer.Marker();
            mapa.addLayer(layer).enable();
            var mCoord = SMap.Coords.fromWGS84(xcoord, ycoord);
            var mOpt = {
                anchor: {left:14, top:49},
                url:SMap.CONFIG.img + "/marker/balloon-3.png"
            };
            var marker = new SMap.Marker(mCoord,"MyMarker", mOpt);
            layer.addMarker(marker);
            var scale = new SMap.Control.Scale();
            mapa.addControl(scale, {left:"8px", bottom:"25px"});
            var msOpt = SMap.MOUSE_PAN;// | SMap.MOUSE_WHEEL | SMap.MOUSE_ZOOM;
            var mouse = new SMap.Control.Mouse(msOpt);
            mapa.addControl(mouse);
            var keyboard = new SMap.Control.Keyboard(SMap.KB_PAN | SMap.KB_ZOOM);
            mapa.addControl(keyboard);
            var selection = new SMap.Control.Selection(2);
            mapa.addControl(selection);
            var zn = new SMap.Control.ZoomNotification();
            mapa.addControl(zn);
            var zoomOpt = {showZoomMenu:false};
            var zoom = new SMap.Control.Zoom(null,{titles:["Přiblížit", "Oddálit"], showZoomMenu:false});
            mapa.addControl(zoom, {right:"2px", top:"10x"});

            var sync = new SMap.Control.Sync({bottomSpace:30});
            mapa.addControl(sync);
        };

        var dekoduj = function(geocoder) {
            var results = geocoder.getResults();

            xcoord = results[0].results[0].coords.x;
            ycoord = results[0].results[0].coords.y;

            createMap();
        };

        var geo = new SMap.Geocoder('<?= $ulice ?>, <?= $mesto ?>, <?= $psc ?>', dekoduj);
    </script>

    <div class="container" id="copyright">
        <p class="pull-left">&copy; Lukáš Senohrábek | Senovo.cz <?= date('Y') ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
