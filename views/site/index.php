<?php

/**
 * @var $this yii\web\View
 * @var $mSettings Settings
 * @var $aArticles Article[]
 * @var $aCarousel array
 * @var $aReferences Reference[]
 */

use app\models\article\Article;
use app\models\Reference;
use app\models\Settings;
use yii\bootstrap\Html;

$this->title = 'Senovo.cz | PC sestavy, HW & SW & Sítě, Programování, weby a infromační systémy';
?>

<div class="site-index">
    <div class="body-carousel">
        <div class="container">
            <div class="macbook">
                <?= \yii\bootstrap\Carousel::widget([
                    'id' => 'carousel-hp',
                    'items' => $aCarousel,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="body-content">
        <?php
        foreach ($aArticles as $mArticle) {
//            echo $this->render('_article', [
//                'mArticle' => $mArticle
//            ]);
            echo \app\components\widgets\article\ArticleWidget::widget(['model' => $mArticle]);
        }
        ?>
    </div>

    <div class="body-reference" id="reference">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading-buttons">
                        <h2>Reference</h2>
                        <?php
                        echo Html::a('Nahoru ' . Html::icon('arrow-up'), '#', ['data-scroll' => 'top'])
                        ?>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div id="reference-wrapper">
            <?php
            $keys = array_keys($aReferences);

            for ($i = 0; $i < $mSettings->getInitialReferencesCount(); $i++) {
                $reference_pk = $keys[$i];

                echo $this->render('_reference', [
                    'mReference' => $aReferences[$reference_pk]
                ]);
            }

            if (count($aReferences) > $mSettings->getInitialReferencesCount()) {
            ?>
            </div>

            <div class="row">
                <div class="col-md-12 text-center">
                    <?= Html::button('Zobrazit více', ['class' => 'btn btn-info', 'id' => 'more-references']) ?>
                </div>
            </div>
            <?php
            }
            ?>
        </div>
    </div>
</div>
