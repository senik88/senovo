<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 03.01.2016
 * Time: 21:47
 *
 * @var Reference $mReference
 */

use app\assets\FancyBoxAsset;
use app\models\File;
use app\models\reference\Reference;
use yii\bootstrap\Html;

FancyBoxAsset::register($this);
?>

<div class="row reference">
    <div class="col-md-12">
        <div class="reference-image">
            <!-- otaceni obrazku - https://davidwalsh.name/css-flip -->
            <img src="<?= $mReference->getType()->getIconFullPath() ?>">
        </div>
        <div class="reference-content">
            <p>
                <b><?= Yii::$app->formatter->asDate($mReference->ref_date) ?></b>
            </p>
            <?= $mReference->content ?>

            <?php
            if ($mReference->link != null) {
                echo Html::tag('p', Html::a($mReference->link, $mReference->link, ['target' => '_blank']));
            }
            ?>

            <div class="row">
                <div class="col-md-12 reference-gallery-wrapper">
                    <?php if ($mReference->hasGallery()): ?>
                        <?php /** @var \app\models\photo\Photo $photo */ ?>
                        <?php foreach ($mReference->gallery->photos as $photo): ?>
                            <?php
                            $thumb = $photo->thumbnail;
                            $file = $photo->getFirstLargestFile();

                            echo Html::a(
                                Html::img(['/site/file', 'hash' => $thumb->hash], ['class' => 'img-thumbnail']),
                                ['/site/file', 'hash' => $file->hash],
                                ['data-fancybox' => $mReference->gallery->photogallery_id, 'rel' => $mReference->gallery->photogallery_id]
                            );
                            ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>