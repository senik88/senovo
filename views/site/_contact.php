<?php
/**
 * @var ContactForm $model
 */

use app\models\ContactForm;
use yii\bootstrap\ActiveForm;

$form = ActiveForm::begin([
    'id' => 'contact-form',
    'action' => ['/site/submit'],
    'method' => 'POST'
]);
?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput([/*'placeholder' => $model->getAttributeLabel('name')*/]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'email')->textInput([/*'placeholder' => $model->getAttributeLabel('email')*/]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'body')->textarea([/*'placeholder' => $model->getAttributeLabel('body')*/]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="g-recaptcha" data-sitekey="<?= Yii::$app->params['recaptcha']['site-key'] ?>"></div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                <?= \yii\bootstrap\Html::submitButton('Odeslat', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>

<?php
ActiveForm::end();