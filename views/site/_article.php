<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 03.01.2016
 * Time: 21:47
 *
 * @var Article $mArticle
 */

use app\models\article\Article;
use yii\bootstrap\Html;
?>

<article class="bg1" id="<?= $mArticle->article_id ?>">
    <div class="row">
        <div class="article-content">
            <div class="arrow"></div>
            <div class="article-text">
                <div class="heading-buttons">
                    <h3><?= $mArticle->title ?></h3>
                    <?php
                    if ($mArticle->show_up) {
                        echo Html::a('Nahoru ' . Html::icon('arrow-up'), '#', ['data-scroll' => 'top']);
                    }
                    ?>
                    <div class="clearfix"></div>
                </div>

                <?= $mArticle->content ?>
            </div>
        </div>
        <div class="article-image"></div>
    </div>
    <div class="spacer"></div>
</article>