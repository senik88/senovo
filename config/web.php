<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'senovo.cz',
    'name' => 'Senovo.cz',
    'language' => 'cs-CZ',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'ulACKkb57nru4IomHy_ky_WiJopQKmvm',
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            //'urlFormat' => 'path',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                // your rules go here // klasickej preklad url
                // todo pujde to nejak s kotvama?
                'sestavy' => 'site/sestavy',
                'poradenstvi' => 'site/poradenstvi',
                'weby' => 'site/weby',
                'reference' => 'site/reference',
                'kontakt' => 'site/kontakt',

                // ---
                'api/v<ver:\d>' => 'api/default/index',

                // ---
                'cms/dashboard' => 'cms/default/index',
                'cms/<controller:\w+>/<action:\w+>' => 'cms/<controller>/<action>',

                'index' => 'site/index',
                //'/' => 'site/index'
            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['/cms/default/login']
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'formatter' => [
            'dateFormat' => 'php:j.n.Y',
            'datetimeFormat' => 'php:j.n.Y H:i:s',
            'decimalSeparator' => ',',
            'thousandSeparator' => '&nbsp;',
            'currencyCode' => 'Kč',
        ],
        /*'view' => [
            'theme' => [
                'basePath' => '@app',
                'baseUrl' => '@web',
                'pathMap' => [
                    '@app/views' => '@app/views',
                    '@app/modules' => '@app/modules'
                ],
            ],
        ],*/
    ],
    'modules' => [
        'cms' => [
            'class' => 'app\modules\cms\CmsModule'
        ],
        'api' => [
            'class' => 'app\modules\api\ApiModule'
        ]
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
