<?php

use yii\db\Migration;

/**
 * Class m200309_183344_GalleryInstall
 */
class m200309_183344_GalleryInstall extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('
            CREATE TABLE "photogallery"(
                "description" Text,
                "published" Timestamp with time zone,
                "photogallery_pk" Serial NOT NULL,
                "photogallery_id" Text NOT NULL,
                "name" Text NOT NULL,
                "sizes" json not null
            )
        ');

        $this->execute('ALTER TABLE "photogallery" ADD CONSTRAINT "ix_photogallery_pk" PRIMARY KEY ("photogallery_pk")');

        $this->execute('
            CREATE TABLE "photo"(
                "photo_pk" Serial NOT NULL,
                "file_small_pk" Integer,
                "file_medium_pk" Integer,
                "file_large_pk" Integer,
                "file_original_pk" Integer,
                "file_thumbnail_pk" Integer,
                "photogallery_pk" Integer,
                "name" Text,
                "description" Text,
                "original_width" Integer NOT NULL,
                "original_height" Integer NOT NULL
            )
        ');

        $this->execute('CREATE INDEX "ix_photo_photogallery_pk" ON "photo" ("photogallery_pk")');
        $this->execute('CREATE INDEX "ix_photo_file_small_pk" ON "photo" ("file_small_pk")');
        $this->execute('CREATE INDEX "ix_photo_file_medium_pk" ON "photo" ("file_medium_pk")');
        $this->execute('CREATE INDEX "ix_photo_file_large_pk" ON "photo" ("file_large_pk")');
        $this->execute('CREATE INDEX "ix_photo_file_original_pk" ON "photo" ("file_original_pk")');
        $this->execute('CREATE INDEX "ix_photo_file_thumbnail_pk" ON "photo" ("file_thumbnail_pk")');
        $this->execute('ALTER TABLE "photo" ADD CONSTRAINT "ix_photo_pk" PRIMARY KEY ("photo_pk")');

        $this->execute('ALTER TABLE "photo" ADD CONSTRAINT "ix_photo_photogallery_pk" FOREIGN KEY ("photogallery_pk") REFERENCES "photogallery" ("photogallery_pk") ON DELETE RESTRICT ON UPDATE RESTRICT');
        $this->execute('ALTER TABLE "photo" ADD CONSTRAINT "ix_photo_file_small_pk" FOREIGN KEY ("file_small_pk") REFERENCES "file" ("file_pk") ON DELETE RESTRICT ON UPDATE RESTRICT');
        $this->execute('ALTER TABLE "photo" ADD CONSTRAINT "ix_photo_file_medium_pk" FOREIGN KEY ("file_medium_pk") REFERENCES "file" ("file_pk") ON DELETE RESTRICT ON UPDATE RESTRICT');
        $this->execute('ALTER TABLE "photo" ADD CONSTRAINT "ix_photo_file_large_pk" FOREIGN KEY ("file_large_pk") REFERENCES "file" ("file_pk") ON DELETE RESTRICT ON UPDATE RESTRICT');
        $this->execute('ALTER TABLE "photo" ADD CONSTRAINT "ix_photo_file_original_pk" FOREIGN KEY ("file_original_pk") REFERENCES "file" ("file_pk") ON DELETE RESTRICT ON UPDATE RESTRICT');
        $this->execute('ALTER TABLE "photo" ADD CONSTRAINT "ix_photo_file_thumbnail_pk" FOREIGN KEY ("file_thumbnail_pk") REFERENCES "file" ("file_pk") ON DELETE RESTRICT ON UPDATE RESTRICT');

        $this->execute('ALTER TABLE article ADD COLUMN photogallery_pk INTEGER');
        $this->execute('CREATE INDEX "ix_article_photogallery_pk" ON "article" ("photogallery_pk")');
        $this->execute('ALTER TABLE "article" ADD CONSTRAINT "ix_article_photogallery_pk" FOREIGN KEY ("photogallery_pk") REFERENCES "photogallery" ("photogallery_pk") ON DELETE RESTRICT ON UPDATE RESTRICT');

        $this->execute('alter table reference add column photogallery_pk integer');
        $this->execute('CREATE INDEX "ix_reference_photogallery_pk" ON "reference" ("photogallery_pk")');
        $this->execute('ALTER TABLE "reference" ADD CONSTRAINT "ix_reference_photogallery_pk" FOREIGN KEY ("photogallery_pk") REFERENCES "photogallery" ("photogallery_pk") ON DELETE RESTRICT ON UPDATE RESTRICT');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute('alter table article drop column photogallery_pk');
        $this->execute('alter table reference drop column photogallery_pk');
        $this->execute('drop table photo');
        $this->execute('drop table photogallery');
    }
}
