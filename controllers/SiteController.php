<?php

namespace app\controllers;

use app\models\File;
use app\models\Homepage;
use app\models\Reference;
use app\models\Settings;
use Yii;
use yii\base\Application;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\ContactForm;
use yii\web\Response;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $mHomepage = new Homepage();
        $mSettings = Settings::getForAccount('senovo');

        $aArticles = $mHomepage->getArticles();
        $aCarousel = $mHomepage->getCarousel();
        $aReferences = $mHomepage->getReferences();

        $model = new ContactForm();

        /*if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }*/

        $this->view->params['contactForm'] = $model;

        return $this->render('index', [
            'mSettings' => $mSettings,
            'aArticles' => $aArticles,
            'aCarousel' => $aCarousel,
            'aReferences' => $aReferences
        ]);
    }

    /**
     * @param $offset
     * @param int $limit
     * @return string
     */
    public function actionReference($offset, $limit = 3)
    {
        $mReference = new Reference();

        $response = [
            'html' => ''
        ];

        $aReference = $mReference->search($offset, $limit);

        foreach ($aReference as $model) {
            $response['html'] .= $this->renderPartial('_reference', [
                'mReference' => $model
            ]);
        }

        $response['more'] = $mReference->hasMoreReferences ? 1 : 0;

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $response;
    }

    /**
     * @param $hash
     */
    public function actionFile($hash)
    {
        /** @var File $mFile */
        $mFile = File::findOne(['hash' => $hash]);
        if ($mFile != null) {
            $file = $mFile->getAbsolutePath() . DIRECTORY_SEPARATOR . $mFile->path . DIRECTORY_SEPARATOR . $mFile->file_pk;

            header("Content-Description: File Transfer");
            header("Content-Type: application/octet-stream");
            header("Content-Disposition: attachment; filename=" . basename($mFile->name));

            readfile ($file);
            exit();
        }
    }

    /**
     * překopat !!! na ajax se ptát až při odpovědi...
     */
    public function actionSubmit()
    {
        $request = Yii::$app->request;
        if (!$request->getIsAjax()) {
            //Yii::$app->session->setFlash('warning')

            return $this->redirect(['/site/index']);
        }  else {
            $response = [];

            if (!$request->getIsPost()) {
                $response['error'] = "Neplatný požadavek!";
            } else {

                $post = $request->post();

                var_dump($post); die;

                $mForm = new ContactForm();
                $recipients = [
                    ['lukas.senohrabek@email.cz' => 'Lukáš Senohrábek']
                ];

                if ($mForm->load($post) && $mForm->contact($recipients)) {
                    $response['success'] = 1;
                } else {
                    //$errors = $mForm->getFirstErrors();
                    $response['error'] = "Chyba při odesílání emailu, opakujte akci později.";
                }
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            return $response;
        }
    }
}
